import numpy as np
import pandas as pd
import os

pd.set_option("display.max_rows", 500)
pd.set_option("display.max_columns", 500)

print("Running...")

### pull in living list for contract comparison
df_ll = pd.read_csv(
    r"A:\data\csv\Reporting LL.csv",
    low_memory=False,
)
df_ll.columns = df_ll.columns.str.strip().str.lower().str.replace(" ", "_")
df_ll.drop(
    columns=[
        "club_types",
        "sc_new_types",
        "sc_new_type",
        "club_type",
        "poach_in_grader",
        "poach_out_grader",
        "contract_end_date_2",
        "renewal_status_2",
        "poach_out_department",
        "poach_out_date",
        "poach_in_date",
        "poach_out_dollars",
        "poach_in_dollars",
        "active_year",
        "date_year",
        "date_qtr",
        "target_year",
        "target_qtr",
        "type",
        "order_type",
        "status",
        "csm",
        "sales_order_advisor",
        "booking_t1",
        "booking_t2",
        "booking_t3",
        "wk_new_commit",
        "wk_rnw_commit",
        "wk_smrcommit",
        "target_date",
        "newmember_monthly_t2",
        "newmember_t2",
        "newmember_t1",
        "newmember_t3",
#        "onboarded",
        "subscription_id",
#        "onboard_detector",
        "active_since",
        "contract_end_date",
        "cancellation_date",
        "cancellation_reason",
        "renewal_status",
        "sales_order_amount",
        "seminar_bundle",
        "bundled_seminar",
        "paying_status",
        "study_club_group",
        "customer_role",
        "renewal_order_#",
        "renewal_order_amount",
        "previous_order",
        "previous_order_amount",
        "masters_program_sc",
        "poach_in_department",
        "previous_order_key",
        "department",
#        "onboard_call_duration",
        "customer_total",
        "rating",
        "risk_level",
        "sales_order_date",
        "payment_plan",
        "poach_out_department_order",
        "billing_freq",
        "poach_in_department_order",
        "item",
        "osonboarded",
        "onboarded_external_id",
        "poach_out_department_new",
        "renewal_status_new",
        "current_msa",
        "original_t1-1",
        "original_t1",
        "original_t2",
        "original_t2-1",
        "original_t3-1",
        "original_t3",
        "forecast_status",
        "forecastrenewalstatus",
        "customer:_lead_source",
    ],
    inplace=True,
)

df_ll.columns
df_ll.shape

df_ll = df_ll[
    [
        "customer",
        "customer_internal_id",
        "customer_category",
        "contract_start_date",
        "sales_order_number",
        "sales_order_internal_id",
        "product",
        "product_order",
        "date",
        "ending_msa",
        "active/not_active",
        "chs_color",
        "chs_score_color",
    ]
]

df_ll = df_ll[~df_ll["product"].str.contains("CEREC")]
df_ll = df_ll[~(df_ll["product"] == "Products")]
df_ll.groupby(["product_order", "product"])["customer_internal_id"].nunique()
df_ll.columns

df_ll["contract_start_date"] = pd.to_datetime(df_ll["contract_start_date"])
df_ll.dropna(subset=["contract_start_date"], inplace=True)

df_ll_count = (
    df_ll.groupby(
        ["customer_internal_id", "active/not_active", "product_order", "product"]
    )["contract_start_date"]
    .count()
    .reset_index()
)

df_ll_count.rename(columns={"contract_start_date": "contract_flag"}, inplace=True)
df_ll_count["contract_flag"] = df_ll_count["contract_flag"].clip(upper=1)
df_ll_count["contract_flag"] = np.where(
    (df_ll_count["active/not_active"] == "Not Active")
    & (df_ll_count["contract_flag"] == 1),
    -1,
    df_ll_count["contract_flag"],
)

df_ll_pivot = df_ll_count.pivot_table(
    values="contract_flag", index=["customer_internal_id"], columns="product"
).reset_index()

df_ll_pivot = df_ll_pivot.fillna(0)
df_ll_pivot.columns
df_ll_pivot.shape

df_ll_pivot = df_ll_pivot[
    [
        "customer_internal_id",
        "Spear Practice Solutions",
        "Faculty Club",
        "Study Clubs",
        "Spear Online",
    ]
]

products = [
    "Spear Practice Solutions",
    "Faculty Club",
    "Study Clubs",
    "Spear Online",
]
for n in products:
    df_ll_pivot[n] = df_ll_pivot[n].astype(int)
    df_ll_pivot[n] = np.where(
        df_ll_pivot[n] >= 1,
        "Active",
        np.where(df_ll_pivot[n] == 0, "No Contract", "Inactive"),
    )


## customer data for matching
df_cust = pd.read_csv(
    r"A:\data\csv\expCustomers.csv",
    low_memory=False,
)
df_cust.columns = df_cust.columns.str.strip().str.lower().str.replace(" ", "_")
df_cust["email"] = df_cust["email"].str.lower()
df_cust["phone"] = df_cust["phone"].str.replace("\\D+", "")

df_cust.columns

df_cust = df_cust[
    [
        "customer_internal_id",
        "spear_id",
        "first_name",
        "last_name",
        "email",
        "alt_email",
        "phone",
        "home_phone",
        "mobile_phone",
        "msa",
    ]
]

df_cust.rename(
    columns={"first_name": "customer_first_name", "last_name": "customer_last_name",},
    inplace=True,
)
df_cust = df_cust.merge(df_ll_pivot, how="left", on="customer_internal_id")

df_cust.drop_duplicates(inplace=True)
df_cust.columns
df_cust.shape


### pull webinar data and clean
df_webinar = pd.read_excel(
    r"A:\data\excel\Webinar Leads Data\Webinar Attendance Data.xlsx",
    sheet_name="Webinar List",
)
df_web_arc = df_webinar.copy()

df_webinar = df_web_arc.copy()

df_webinar.columns = df_webinar.columns.str.strip().str.lower().str.replace(" ", "_")
df_webinar.rename(
    columns={"time_in_session_(minutes)": "minutes_in_webinar"}, inplace=True
)

df_webinar["email"] = df_webinar["email"].str.lower().str.strip()
df_webinar["phone_number"] = df_webinar["phone_number"].str.replace("\\D+", "")

df_webinar = df_webinar[df_webinar["attended"] == "Yes"]
df_webinar["content_category"] = "Webinar"
df_webinar.columns = df_webinar.columns.str.replace("webinar_", "content_")

df_webinar["email"].fillna(value=0, inplace=True)
df_webinar["phone_number"].fillna(value=0, inplace=True)
df_webinar["minutes_in_webinar"].fillna(value=0, inplace=True)

df_webinar = (
    df_webinar.groupby(
        [
            "content_category",
            "content_group",
            "content_name",
            "content_date",
            "first_name",
            "last_name",
            "email",
            "phone_number",
        ]
    )["minutes_in_webinar"]
    .sum()
    .reset_index()
)
df_webinar.shape
df_webinar["email"].nunique()

df_webinar_main = df_webinar.merge(df_cust, how="left", on="email")
df_webinar_main = (
    df_webinar_main.groupby(
        [
            "content_category",
            "content_group",
            "content_name",
            "content_date",
            "first_name",
            "last_name",
            "email",
            "phone_number",
            "minutes_in_webinar",
        ]
    )
    .first()
    .reset_index()
)

df_webinar_alt = df_webinar_main[pd.isnull(df_webinar_main["customer_internal_id"])]
df_webinar_main = df_webinar_main[~pd.isnull(df_webinar_main["customer_internal_id"])]

df_webinar.shape
df_webinar_alt.shape[0] + df_webinar_main.shape[0]

df_webinar_alt.drop(
    columns=[
        "customer_internal_id",
        "spear_id",
        "customer_first_name",
        "customer_last_name",
        "alt_email",
        "phone",
        "home_phone",
        "mobile_phone",
        "msa",
        "Spear Practice Solutions",
        "Faculty Club",
        "Study Clubs",
        "Spear Online",
    ],
    inplace=True,
)

df_webinar_alt.shape

df_alt_cust = df_cust.drop(columns=["email"])
df_webinar_alt = df_webinar_alt.merge(
    df_alt_cust, how="left", left_on="email", right_on="alt_email"
)
df_webinar_alt = (
    df_webinar_alt.groupby(
        [
            "content_category",
            "content_group",
            "content_name",
            "content_date",
            "first_name",
            "last_name",
            "email",
            "minutes_in_webinar",
        ]
    )
    .first()
    .reset_index()
)
df_webinar_alt.shape

df_webinar_export = df_webinar_main.append(df_webinar_alt)
df_webinar_export.shape

df_webinar_phone = df_webinar_export[
    (
        pd.isnull(df_webinar_export["customer_internal_id"])
        & (df_webinar_export["phone_number"] != 0)
    )
]
df_webinar_export = df_webinar_export[
    (
        (pd.isnull(df_webinar_export["customer_internal_id"]) == False)
        | (df_webinar_export["phone_number"] == 0)
    )
]

df_webinar_phone.shape
df_webinar_export.shape

df_webinar_phone.drop(
    columns=[
        "customer_internal_id",
        "spear_id",
        "customer_first_name",
        "customer_last_name",
        "alt_email",
        "phone",
        "home_phone",
        "mobile_phone",
        "msa",
        "Spear Practice Solutions",
        "Faculty Club",
        "Study Clubs",
        "Spear Online",
    ],
    inplace=True,
)

df_webinar_phone.merge(df_cust, how="left", left_on="phone_number", right_on="phone")
df_webinar_phone = (
    df_webinar_phone.groupby(
        [
            "content_category",
            "content_group",
            "content_name",
            "content_date",
            "first_name",
            "last_name",
            "email",
            "minutes_in_webinar",
        ]
    )
    .first()
    .reset_index()
)

df_webinar_export = df_webinar_export.append(df_webinar_phone)
df_webinar_export.shape
df_webinar_export["email"].nunique()

df_webinar_export.columns
df_webinar_export["content_completed"] = 1

### pull in recorded video and clean
df_video = pd.read_csv(
    r"A:\data\csv\PRI Lessons.csv",
    low_memory=False,
)
df_video.columns = df_video.columns.str.strip().str.lower().str.replace(" ", "_")
df_video.columns = df_video.columns.str.replace("user_", "")
df_video.columns = df_video.columns.str.replace("lesson_", "content_")
df_video.drop(columns=["course_title"], inplace=True)
df_video.rename(
    columns={"content_title": "content_name", "content_id": "lesson_id"}, inplace=True
)

df_video["content_category"] = "Lesson"
df_video["content_group"] = "Practice Recovery"
df_video["registration_time"] = 0
df_video["minutes_in_webinar"] = 0

df_video.rename(
    columns={
        "created_at": "content_date",
        "netsuite_id": "customer_internal_id",
        "phone": "phone_number",
    },
    inplace=True,
)

df_video["phone"] = df_video["phone_number"].str.replace("\\D+", "")
df_video["mobile"] = df_video["mobile"].str.replace("\\D+", "")

df_video.columns
df_video.shape

df_video = df_video[
    [
        "content_category",
        "content_group",
        "content_name",
        "content_date",
        "first_name",
        "last_name",
        "email",
        "phone_number",
        "minutes_in_webinar",
        "content_completed",
        "customer_internal_id",
    ]
].copy()

df_video.columns

df_video["content_date"] = pd.to_datetime(df_video["content_date"]).dt.round("d")
df_video.groupby(["content_date", "content_name",])["email"].nunique()

df_video.drop_duplicates(inplace=True)

df_video_known = df_video[pd.isnull(df_video["customer_internal_id"]) == False].copy()
df_video_unknown = df_video[pd.isnull(df_video["customer_internal_id"]) == True].copy()

df_video_known["customer_internal_id"] = df_video_known["customer_internal_id"].astype(
    int
)
df_video_known = df_video_known.merge(df_cust, how="left", on="customer_internal_id")

df_video_known.shape[0] + df_video_unknown.shape[0]

df_video_unknown.drop(
    columns=["customer_internal_id",], inplace=True,
)

df_video_email_match = df_video_unknown.merge(
    df_cust, how="left", left_on="email", right_on="email"
)

df_video_export = df_video_known.append(df_video_email_match)

df_video.shape
df_video_export.shape
df_video_export.columns

df_video_export["phone_number"].fillna(value=0, inplace=True)
df_video_export = (
    df_video_export.groupby(
        [
            "content_category",
            "content_group",
            "content_name",
            "content_date",
            "first_name",
            "last_name",
            "email",
            "phone_number",
            "minutes_in_webinar",
            "content_completed",
        ]
    )
    .first()
    .reset_index()
)

df_export = df_webinar_export.append(df_video_export)

df_export["content_name"] = np.where(
    df_export["content_name"] == "Recovery Budget & Strategy",
    "Practice Recovery Workshop #1: Recovery Budget & Analysis",
    df_export["content_name"],
)
df_export = df_export[df_export["email"].str.contains("@speareducation.com") == False]
os.remove(r"A:\data\python_data\courses\pri_webinars_lessons.csv")
df_export.to_csv(r"A:\data\python_data\courses\pri_webinars_lessons.csv")

print("Complete")

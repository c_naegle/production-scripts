# ISSUES
# 1. cust id from ll doesn't account for folks that have no subscriptions - big issue
#       a. shift id to cust_id in cust, adopt a cust-first model
# 2. new sub logic unwritten

### look at time in session is a column for virtual seminar

import numpy as np
import pandas as pd
import os

pd.set_option("display.max_rows", 500)
pd.set_option("display.max_columns", 500)

print("Running...")

## expCustomers
df_cust = pd.read_csv(r"A:\data\csv\expCustomers.csv", low_memory=False)
df_cust.columns = df_cust.columns.str.strip().str.lower().str.replace(" ", "_")
df_cust.columns

## ll master
df_ll = pd.read_csv(r"A:\data\csv\Reporting LL.csv", low_memory=False)
df_ll.columns = df_ll.columns.str.strip().str.lower().str.replace(" ", "_")
df_ll.columns

## expCourseAttendees
df_course = pd.read_csv(r"A:\data\csv\expCourseAttendees.csv", low_memory=False)
df_course.columns = df_course.columns.str.strip().str.lower().str.replace(" ", "_")
df_course.columns

## phone data
import psycopg2
import sqlalchemy
import datetime

database = "spear_edw"
user = "ceops"
password = "C30perations88@99"
host = "edm-redshift-prod-1.ccvpsxk3s37e.us-east-1.redshift.amazonaws.com"
port = "5439"
schema = "ceops"

connection_string = "redshift+psycopg2://%s:%s@%s:%s/%s" % (
    user,
    password,
    host,
    str(port),
    database,
)
engine = sqlalchemy.create_engine(
    connection_string, connect_args={"options": "-csearch_path={}".format(schema)}
)

df_phone = pd.read_sql_query(
    """
	SELECT *
	FROM phone_call
	WHERE startdate > '2020-03-16'
    """,
    engine,
)

df_phone.columns = df_phone.columns.str.strip().str.lower().str.replace(" ", "_")
df_phone.columns

# DPMS data
df_sps = pd.read_sql_query(
    """
	SELECT *
	FROM sps_opportunities
    """,
    engine,
)

df_sps.columns = df_sps.columns.str.strip().str.lower().str.replace(" ", "_")
df_sps.columns

### CUST PREP
# cust needs to be spine for demo
df_spine = df_cust[
    ["customer_internal_id", "name", "phone", "email", "state/_province"]
].copy()
df_spine.rename(columns={"state/_province": "state/province"}, inplace=True)
df_spine.head()

### COURSE ATTENDEE PREP
df_course.columns
# df_course.groupby(["product", "course_start_date"])["customer_internal_id"].nunique()

# course history
course_products = [
    "CEREC Campus",
    "Masters Workshops",
    "Spear Seminars",
    "Spear Workshops",
    "Summit",
]

df_course_hist = df_course.copy()
df_course_hist["course_start_date"] = pd.to_datetime(
    df_course_hist["course_start_date"]
)
df_course_hist.sort_values(by="course_start_date", ascending=False, inplace=True)
df_course_hist = df_course_hist[
    df_course_hist["course_start_date"] < datetime.datetime.today()
]
df_course_hist = df_course[
    ["customer_internal_id", "product", "course_start_date"]
].copy()

df_course_hist["seminar_date"] = np.where(
    df_course_hist["product"] == "Spear Seminars",
    df_course_hist["course_start_date"],
    pd.NaT,
)
df_course_hist["workshop_date"] = np.where(
    df_course_hist["product"] == "Spear Workshops",
    df_course_hist["course_start_date"],
    pd.NaT,
)
df_course_hist["workshop_date"] = np.where(
    df_course_hist["product"] == "Masters Workshops",
    df_course_hist["course_start_date"],
    df_course_hist["workshop_date"],
)
df_course_hist["CDOCS_date"] = np.where(
    df_course_hist["product"] == "CEREC Campus",
    df_course_hist["course_start_date"],
    pd.NaT,
)
df_course_hist.drop(columns=["product", "course_start_date"], inplace=True)
df_course_hist.drop_duplicates(inplace=True)

# add virtual seminars
df_virtual = df_course[df_course["campus_event"].str.contains("Virtual")]
df_virtual = df_virtual[
    [
        "customer_internal_id",
        "course_group",
        "course_start_date",
        "campus_event",
        "course_id",
        "attendee_category",
        "attendee_internal_id",
        "attendee_name",
        "campus_sales_order_amount",
    ]
]

df_virtual = (
    df_virtual.groupby(
        [
            "customer_internal_id",
            "course_group",
            "course_start_date",
            "campus_event",
            "course_id",
            "attendee_category",
            "attendee_internal_id",
            "attendee_name",
        ]
    )["campus_sales_order_amount"]
    .sum()
    .reset_index()
)
df_virtual.drop(columns=["campus_sales_order_amount"], inplace=True)

df_virtual.shape
df_export = df_virtual.merge(df_spine, how="left", on="customer_internal_id")
df_export.shape

### LIVING LIST PREP
spear_order = {
    "Spear Practice Solutions": 1,
    "Masters Program": 2,
    "Faculty Club": 3,
    "Study Clubs": 4,
    "Spear Online": 5,
}
cerec_order = {
    "CEREC Mentor Group": 1,
    "CEREC Premium Plus": 2,
    "CEREC Academy Membership": 3,
    "CEREC Study Club": 4,
}
df_ll["spear_main_product"] = df_ll["product"].map(spear_order)
df_ll["cerec_main_product"] = df_ll["product"].map(cerec_order)
df_ll["spear_main_product"].fillna(9, inplace=True)
df_ll["cerec_main_product"].fillna(9, inplace=True)

df_demo = df_ll.copy()
df_demo = (
    df_demo.groupby(["customer_internal_id", "customer_category",])
    .agg({"spear_main_product": "min", "cerec_main_product": "min",})
    .reset_index()
)
df_demo.rename(columns={"customer_category": "role/specialty"}, inplace=True)

spear_order = {v: k for k, v in spear_order.items()}
cerec_order = {v: k for k, v in cerec_order.items()}
df_demo["spear_main_product"] = df_demo["spear_main_product"].map(spear_order)
df_demo["cerec_main_product"] = df_demo["cerec_main_product"].map(cerec_order)

# applicable msas
df_ll["contract_end_date"] = pd.to_datetime(df_ll["contract_end_date"])
df_demo_join = df_ll[
    ["customer_internal_id", "product", "chs_color", "contract_end_date",]
].copy()
df_demo_join["chs_color"] = df_demo_join["chs_color"].fillna("N/A")
df_demo_join = (
    df_demo_join.groupby(["customer_internal_id", "product", "chs_color",])[
        "contract_end_date"
    ]
    .max()
    .reset_index()
)
df_demo_join.drop_duplicates(inplace=True)

df_demo.shape
df_demo = df_demo.merge(
    df_demo_join,
    how="left",
    left_on=["customer_internal_id", "spear_main_product"],
    right_on=["customer_internal_id", "product"],
)
df_demo.drop(columns=["product"], inplace=True)
df_demo.rename(
    columns={
        "chs_color": "spear_chs_color",
        "contract_end_date": "spear_contract_end_date",
    },
    inplace=True,
)
df_demo = df_demo.merge(
    df_demo_join,
    how="left",
    left_on=["customer_internal_id", "cerec_main_product"],
    right_on=["customer_internal_id", "product"],
)
df_demo.drop(columns=["product", "chs_color"], inplace=True)
df_demo.rename(
    columns={"contract_end_date": "cerec_contract_end_date",}, inplace=True,
)

# multiple study club join
df_demo_join = df_ll[["customer_internal_id", "study_club_group"]].copy()
df_demo_join.rename(columns={"study_club_group": "study_club_groups"}, inplace=True)
df_demo_join.dropna(inplace=True)
df_demo_join.drop_duplicates(inplace=True)
df_demo_join = (
    df_demo_join.groupby("customer_internal_id")["study_club_groups"]
    .apply(lambda x: " | ".join(x))
    .reset_index()
)

df_demo = df_demo.merge(df_demo_join, how="left", on="customer_internal_id")

df_demo_join = df_ll[df_ll["product"] == "Study Clubs"]
df_demo_join = df_ll[["customer_internal_id", "ending_msa"]].copy()
df_demo_join.rename(columns={"ending_msa": "study_club_msas"}, inplace=True)
df_demo_join.dropna(inplace=True)
df_demo_join.drop_duplicates(inplace=True)
df_demo_join = (
    df_demo_join.groupby("customer_internal_id")["study_club_msas"]
    .apply(lambda x: " | ".join(x))
    .reset_index()
)

df_demo = df_demo.merge(df_demo_join, how="left", on="customer_internal_id")
df_demo.head()

# multiple msas
df_demo_join = df_ll[["customer_internal_id", "product", "ending_msa",]].copy()
df_demo_join.rename(columns={"ending_msa": "ending_msas"}, inplace=True)
df_demo_join.dropna(inplace=True)
df_demo_join.drop_duplicates(inplace=True)
df_demo_join = (
    df_demo_join.groupby(["customer_internal_id", "product"])["ending_msas"]
    .apply(lambda x: " | ".join(x))
    .reset_index()
)

df_demo.shape
df_demo = df_demo.merge(
    df_demo_join,
    how="left",
    left_on=["customer_internal_id", "spear_main_product"],
    right_on=["customer_internal_id", "product"],
)
df_demo.rename(columns={"ending_msas": "spear_ending_msas"}, inplace=True)
df_demo.drop(columns=["product"], inplace=True)
df_demo = df_demo.merge(
    df_demo_join,
    how="left",
    left_on=["customer_internal_id", "cerec_main_product"],
    right_on=["customer_internal_id", "product"],
)
df_demo.rename(columns={"ending_msas": "cerec_ending_msas"}, inplace=True)
df_demo.drop(columns=["product"], inplace=True)
df_demo.shape

# multiple paying states
df_demo_join = df_ll[["customer_internal_id", "product", "paying_status",]].copy()
df_demo_join.dropna(inplace=True)
df_demo_join.drop_duplicates(inplace=True)
df_demo_join = (
    df_demo_join.groupby(["customer_internal_id", "product"])["paying_status"]
    .apply(lambda x: " | ".join(x))
    .reset_index()
)

df_demo.shape
df_demo = df_demo.merge(
    df_demo_join,
    how="left",
    left_on=["customer_internal_id", "spear_main_product"],
    right_on=["customer_internal_id", "product"],
)
df_demo.rename(columns={"paying_status": "spear_paying_status"}, inplace=True)
df_demo.drop(columns=["product"], inplace=True)
df_demo = df_demo.merge(
    df_demo_join,
    how="left",
    left_on=["customer_internal_id", "cerec_main_product"],
    right_on=["customer_internal_id", "product"],
)
df_demo.rename(columns={"paying_status": "cerec_paying_status"}, inplace=True)
df_demo.drop(columns=["product"], inplace=True)
df_demo.shape

df_export = df_export.merge(df_demo, how="left", on="customer_internal_id")

## phone data
df_phone["phone"] = df_phone["phone"].str.replace("\\D+", "")

df_phone.columns

obj_columns = list(df_phone.select_dtypes(include=["object"]).columns.values)
df_phone[obj_columns] = df_phone[obj_columns].replace([None], np.nan)
df_phone["company_internalid"].dropna(inplace=True)
df_phone["company_internalid"] = df_phone["company_internalid"].astype(float)

df_phone.rename(
    columns={
        "company_internalid": "customer_internal_id",
        "phone": "call_phone",
        "owner_name": "employee_name",
        "contivio call id": "call_id",
        "startdate": "call_date",
        "contivio call direction": "call_direction",
        "contivio call type": "call_type",
        "contivio disposition": "call_disposition",
        "contivio contact result": "contact_result",
        "contivio recording": "call_recording",
        "contivio call duration in minutes": "call_duration",
    },
    inplace=True,
)

df_phone.columns

df_phone_join = df_phone[
    [
        "customer_internal_id",
        #        "call_phone",
        #        "employee_name",
        #        "call_id",
        "call_date",
        #        "call_direction",
        #        "call_type",
        #        "call_disposition",
        #        "contact_result",
        #        "call_recording",
        #        "call_duration",
    ]
]

df_export.shape

df_phone_count = df_export.merge(df_phone_join, how="left", on="customer_internal_id")
df_phone_count["course_start_date"] = pd.to_datetime(
    df_phone_count["course_start_date"]
)
df_phone_count["call_date"] = np.where(
    df_phone_count["call_date"] >= df_phone_count["course_start_date"],
    df_phone_count["call_date"],
    pd.NaT,
)

df_phone_count = (
    df_phone_count.groupby(
        ["customer_internal_id", "course_group", "campus_event", "course_id",]
    )
    .agg({"call_date": "count"})
    .reset_index()
)

df_phone_count.shape
df_phone_count.columns

df_export = df_export.merge(
    df_phone_count,
    how="left",
    on=["customer_internal_id", "course_group", "campus_event", "course_id",],
)
df_export.rename(columns={"call_date": "call_total"}, inplace=True)

# add course history
df_hist = df_export.merge(df_course_hist, how="left", on="customer_internal_id")

df_hist_save = df_hist.copy()
df_export_save = df_export.copy()
df_hist = df_hist_save.copy()
df_export = df_export_save.copy()

date_cols = ["seminar_date", "workshop_date", "CDOCS_date"]
df_hist["course_start_date"] = pd.to_datetime(df_hist["course_start_date"])

for d in date_cols:
    df_hist[d] = pd.to_datetime(df_hist[d])
    df_hist["most_recent_" + d] = np.where(
        (df_hist[d] < df_hist["course_start_date"])
        & (df_hist[d] <= datetime.datetime.today()),
        df_hist[d],
        pd.NaT,
    )
    df_hist["next_" + d] = np.where(
        df_hist["course_start_date"] < df_hist[d], df_hist[d], pd.NaT
    )
    df_hist["most_recent_" + d] = pd.to_datetime(df_hist["most_recent_" + d])
    df_hist["next_" + d] = pd.to_datetime(df_hist["next_" + d])

past_date_cols = [
    "most_recent_seminar_date",
    "most_recent_workshop_date",
    "most_recent_CDOCS_date",
]
future_date_cols = ["next_seminar_date", "next_workshop_date", "next_CDOCS_date"]

for d in past_date_cols:
    df_hist[d] = df_hist[d].fillna(datetime.date(1678, 1, 1))
    df_hist[d] = pd.to_datetime(df_hist[d])
for d in future_date_cols:
    df_hist[d] = df_hist[d].fillna(datetime.date(2261, 1, 1))
    df_hist[d] = pd.to_datetime(df_hist[d])

df_hist.drop(
    columns=[
        "seminar_date",
        "workshop_date",
        "CDOCS_date",
        #        "customer",
        "role/specialty",
        "spear_main_product",
        "cerec_main_product",
        "spear_chs_color",
        "spear_contract_end_date",
        "cerec_contract_end_date",
        "study_club_groups",
        "study_club_msas",
        "spear_ending_msas",
        "cerec_ending_msas",
        "spear_paying_status",
        "cerec_paying_status",
        "phone",
        "email",
        "state/province",
        "course_group",
        "course_start_date",
        "course_id",
        "attendee_category",
        "attendee_internal_id",
        "attendee_name",
        "call_total",
        "seminar_date",
        "workshop_date",
        "CDOCS_date",
    ],
    inplace=True,
)

df_course_hist_join = (
    df_hist.groupby(["customer_internal_id", "campus_event",])
    .agg(
        {
            "most_recent_seminar_date": "max",
            "next_seminar_date": "min",
            "most_recent_workshop_date": "max",
            "next_workshop_date": "min",
            "most_recent_CDOCS_date": "max",
            "next_CDOCS_date": "min",
        }
    )
    .reset_index()
)

for d in past_date_cols:
    df_course_hist_join[d] = np.where(
        df_course_hist_join[d] < datetime.datetime(1679, 1, 1),
        pd.NaT,
        df_course_hist_join[d],
    )
    df_course_hist_join[d] = pd.to_datetime(df_course_hist_join[d])
for d in future_date_cols:
    df_course_hist_join[d] = np.where(
        df_course_hist_join[d] > datetime.datetime(2260, 1, 1),
        pd.NaT,
        df_course_hist_join[d],
    )
    df_course_hist_join[d] = pd.to_datetime(df_course_hist_join[d])

df_export = df_export.merge(
    df_course_hist_join, how="left", on=["customer_internal_id", "campus_event"]
)
df_export = df_export[pd.isnull(df_export["campus_event"]) == False]
df_export["customer_internal_id"].nunique()

df_export["refresh_date"] = datetime.date.today()

### add DPMS
df_sps_join = df_sps[
    ["customer_internal_id", "dpms", "demo_scheduled_date", "demo_completion_date"]
].copy()
df_sps_join.drop_duplicates(inplace=True)

sps_dates = ["demo_scheduled_date", "demo_completion_date"]
for d in sps_dates:
    df_sps_join[d] = df_sps_join[d].fillna(datetime.date(1678, 1, 1))
    df_sps_join[d] = pd.to_datetime(df_sps_join[d])

df_sps_join = (
    df_sps_join.groupby(["customer_internal_id", "dpms"])
    .agg({"demo_scheduled_date": "max", "demo_completion_date": "max"})
    .reset_index()
)

for d in sps_dates:
    df_sps_join[d] = np.where(
        df_sps_join[d] < datetime.datetime(1679, 1, 1), pd.NaT, df_sps_join[d]
    )
    df_sps_join[d] = pd.to_datetime(df_sps_join[d])

df_export.shape
df_export = df_export.merge(df_sps_join, how="left", on="customer_internal_id")

### add new sales
df_sale = df_ll.copy()
df_sale = df_sale[
    [
        "customer_internal_id",
        "product",
        "sales_order_date",
        "contract_start_date",
        "cancellation_date",
        "contract_end_date",
    ]
]
df_sale.shape
df_sale = df_sale[df_sale["cancellation_date"].isnull()]
df_sale.rename(columns={"customer_internal_id_s": "customer_internal_id"}, inplace=True)

df_sale.columns
df_sale = df_virtual.merge(df_sale, how="left", on="customer_internal_id")
df_sale.shape
df_sale.columns
df_virtual.shape

df_sale["sales_order_date"] = df_sale["sales_order_date"].fillna("01-01-1700")
df_sale["course_start_date"] = df_sale["course_start_date"].fillna("01-01-1700")

df_sale["sales_order_date"] = pd.to_datetime(df_sale["sales_order_date"])
df_sale["course_start_date"] = pd.to_datetime(df_sale["course_start_date"])

df_past = df_sale[df_sale["course_start_date"] < df_sale["sales_order_date"]]
df_past = (
    df_past.groupby(["customer_internal_id", "campus_event", "product"])
    .agg({"sales_order_date": "max"})
    .reset_index()
)
df_past.shape
df_past[:20]

spear_order = {v: k for k, v in spear_order.items()}
cerec_order = {v: k for k, v in cerec_order.items()}
df_past["spear_main_sale"] = df_past["product"].map(spear_order)
df_past["cerec_main_sale"] = df_past["product"].map(cerec_order)
df_past["spear_main_sale"].fillna(9, inplace=True)
df_past["cerec_main_sale"].fillna(9, inplace=True)

df_past = (
    df_past.groupby(["customer_internal_id", "campus_event",])
    .agg({"spear_main_sale": "min", "cerec_main_sale": "min",})
    .reset_index()
)

spear_order = {v: k for k, v in spear_order.items()}
cerec_order = {v: k for k, v in cerec_order.items()}


df_past["spear_main_sale"] = df_past["spear_main_sale"].astype(int).map(spear_order)
df_past["cerec_main_sale"] = df_past["cerec_main_sale"].astype(int).map(cerec_order)

df_export = df_export.merge(
    df_past, how="left", on=["customer_internal_id", "campus_event"]
)

# adding proposal columns
df_export["membership_proposal"] = np.where(
    pd.isnull(df_export["spear_main_product"]), "Spear Online", " "
)
df_export["membership_proposal"] = np.where(
    df_export["spear_main_product"] == "Spear Online",
    "Faculty Club",
    df_export["membership_proposal"],
)
df_export["membership_proposal"] = np.where(
    df_export["spear_main_product"] == "Study Clubs",
    "Faculty Club",
    df_export["membership_proposal"],
)
df_export["workshop_proposal"] = np.where(
    (
        (df_export["role/specialty"] == "General Practitioner")
        & (pd.isnull(df_export["most_recent_workshop_date"]))
    ),
    "FGTP",
    " ",
)
df_export["workshop_proposal"] = np.where(
    pd.isnull(df_export["next_workshop_date"]) & (df_export["workshop_proposal"] != "FGTP"),
    "Next Step",
    df_export["workshop_proposal"],
)

os.remove(r"A:\data\python_data\courses\virtual_seminar_treatment.csv")
df_export.to_csv(r"A:\data\python_data\courses\virtual_seminar_treatment.csv")

print("Complete")

# quesiton for tomorrow - why are there so many null sales order dates!!!


### GENERAL NOTES

# sps opportunity consults
# was saving to sps opportunities
# top spear

# column for attended
# peter is adding perfect match
#   webinars -> seminars
#   yes / null
#   time in session

# on scheduling report
# tell them by state hygiene production and non-hygiene production
# could we build a report in $ produced
# expect a mock-up today

# calculate main product at certain date
# calculate main product today = DONE


###
# Steve R: demographic counts -
# now: send an email to Matt Crego / Aleks / Marcus

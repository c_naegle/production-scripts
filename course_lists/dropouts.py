# ISSUES
# 1. cust id from ll doesn't account for folks that have no subscriptions - big issue
#       a. shift id to cust_id in cust, adopt a cust-first model
# 2. new sub logic unwritten

### look at time in session is a column for virtual seminar

import numpy as np
import pandas as pd
import os

pd.set_option("display.max_rows", 500)
pd.set_option("display.max_columns", 500)

print("Running...")

## dropouts
df_do = pd.read_csv(r"A:\data\csv\dropouts_6.2.2020.csv", low_memory=False)
df_do.columns = df_do.columns.str.strip().str.lower().str.replace(" ", "_")
df_do.columns

## expCustomers
df_cust = pd.read_csv(r"A:\data\csv\expCustomers.csv", low_memory=False)
df_cust.columns = df_cust.columns.str.strip().str.lower().str.replace(" ", "_")
df_cust.columns

## ll master
df_ll = pd.read_csv(r"A:\data\csv\Reporting LL.csv", low_memory=False)
df_ll.columns = df_ll.columns.str.strip().str.lower().str.replace(" ", "_")
df_ll.columns

## expCourseAttendees
df_course = pd.read_csv(r"A:\data\csv\expCourseAttendees.csv", low_memory=False)
df_course.columns = df_course.columns.str.strip().str.lower().str.replace(" ", "_")
df_course.columns

## phone data
import psycopg2
import sqlalchemy
import datetime

database = "spear_edw"
user = "ceops"
password = "C30perations88@99"
host = "edm-redshift-prod-1.ccvpsxk3s37e.us-east-1.redshift.amazonaws.com"
port = "5439"
schema = "ceops"

connection_string = "redshift+psycopg2://%s:%s@%s:%s/%s" % (
    user,
    password,
    host,
    str(port),
    database,
)
engine = sqlalchemy.create_engine(
    connection_string, connect_args={"options": "-csearch_path={}".format(schema)}
)

df_phone = pd.read_sql_query(
    """
	SELECT *
	FROM phone_call
	WHERE startdate > '2020-03-16'
    """,
    engine,
)

df_phone.columns = df_phone.columns.str.strip().str.lower().str.replace(" ", "_")
df_phone.columns

# DPMS data
df_sps = pd.read_sql_query(
    """
	SELECT *
	FROM sps_opportunities
    """,
    engine,
)

df_sps.columns = df_sps.columns.str.strip().str.lower().str.replace(" ", "_")
df_sps.columns

### CUST PREP
# cust needs to be spine for demo
df_spine = df_cust[
    ["customer_internal_id", "name", "phone", "email", "state/_province"]
].copy()
df_spine.rename(columns={"state/_province": "state/province"}, inplace=True)
df_spine.head()

df_export = df_do.merge(df_spine, how="left", on="email")

### COURSE ATTENDEE PREP
df_course.columns
# df_course.groupby(["product", "course_start_date"])["customer_internal_id"].nunique()

# course history
course_products = [
    "CEREC Campus",
    "Masters Workshops",
    "Spear Seminars",
    "Spear Workshops",
    "Summit",
]

df_course_hist = df_course.copy()
df_course_hist["course_start_date"] = pd.to_datetime(
    df_course_hist["course_start_date"]
)
df_course_hist.sort_values(by="course_start_date", ascending=False, inplace=True)
df_course_hist = df_course_hist[
    df_course_hist["course_start_date"] < datetime.datetime.today()
]
df_course_hist = df_course[
    ["customer_internal_id", "product", "course_start_date"]
].copy()

df_course_hist["seminar_date"] = np.where(
    df_course_hist["product"] == "Spear Seminars",
    df_course_hist["course_start_date"],
    pd.NaT,
)
df_course_hist["workshop_date"] = np.where(
    df_course_hist["product"] == "Spear Workshops",
    df_course_hist["course_start_date"],
    pd.NaT,
)
df_course_hist["workshop_date"] = np.where(
    df_course_hist["product"] == "Masters Workshops",
    df_course_hist["course_start_date"],
    df_course_hist["workshop_date"],
)
df_course_hist["CDOCS_date"] = np.where(
    df_course_hist["product"] == "CEREC Campus",
    df_course_hist["course_start_date"],
    pd.NaT,
)
df_course_hist.drop(columns=["product", "course_start_date"], inplace=True)
df_course_hist.drop_duplicates(inplace=True)

### LIVING LIST PREP
spear_order = {
    "Spear Practice Solutions": 1,
    "Masters Program": 2,
    "Faculty Club": 3,
    "Study Clubs": 4,
    "Spear Online": 5,
}
cerec_order = {
    "CEREC Mentor Group": 1,
    "CEREC Premium Plus": 2,
    "CEREC Academy Membership": 3,
    "CEREC Study Club": 4,
}
df_ll["spear_main_product"] = df_ll["product"].map(spear_order)
df_ll["cerec_main_product"] = df_ll["product"].map(cerec_order)
df_ll["spear_main_product"].fillna(9, inplace=True)
df_ll["cerec_main_product"].fillna(9, inplace=True)

df_ll = df_ll[df_ll["active/not_active"] == "Active"]

df_demo = df_ll.copy()
df_demo = (
    df_demo.groupby(["customer_internal_id", "customer_category",])
    .agg({"spear_main_product": "min", "cerec_main_product": "min",})
    .reset_index()
)
df_demo.rename(columns={"customer_category": "role/specialty"}, inplace=True)

spear_order = {v: k for k, v in spear_order.items()}
cerec_order = {v: k for k, v in cerec_order.items()}
df_demo["spear_main_product"] = df_demo["spear_main_product"].map(spear_order)
df_demo["cerec_main_product"] = df_demo["cerec_main_product"].map(cerec_order)

# applicable msas
df_ll["contract_end_date"] = pd.to_datetime(df_ll["contract_end_date"])
df_demo_join = df_ll[
    ["customer_internal_id", "product", "chs_color", "contract_end_date",]
].copy()
df_demo_join["chs_color"] = df_demo_join["chs_color"].fillna("N/A")
df_demo_join = (
    df_demo_join.groupby(["customer_internal_id", "product", "chs_color",])[
        "contract_end_date"
    ]
    .max()
    .reset_index()
)
df_demo_join.drop_duplicates(inplace=True)

df_demo.shape
df_demo = df_demo.merge(
    df_demo_join,
    how="left",
    left_on=["customer_internal_id", "spear_main_product"],
    right_on=["customer_internal_id", "product"],
)
df_demo.drop(columns=["product"], inplace=True)
df_demo.rename(
    columns={
        "chs_color": "spear_chs_color",
        "contract_end_date": "spear_contract_end_date",
    },
    inplace=True,
)
df_demo = df_demo.merge(
    df_demo_join,
    how="left",
    left_on=["customer_internal_id", "cerec_main_product"],
    right_on=["customer_internal_id", "product"],
)
df_demo.drop(columns=["product", "chs_color"], inplace=True)
df_demo.rename(
    columns={"contract_end_date": "cerec_contract_end_date",}, inplace=True,
)

# multiple study club join
df_demo_join = df_ll[["customer_internal_id", "study_club_group"]].copy()
df_demo_join.rename(columns={"study_club_group": "study_club_groups"}, inplace=True)
df_demo_join.dropna(inplace=True)
df_demo_join.drop_duplicates(inplace=True)
df_demo_join = (
    df_demo_join.groupby("customer_internal_id")["study_club_groups"]
    .apply(lambda x: " | ".join(x))
    .reset_index()
)

df_demo = df_demo.merge(df_demo_join, how="left", on="customer_internal_id")

df_demo_join = df_ll[df_ll["product"] == "Study Clubs"]
df_demo_join = df_ll[["customer_internal_id", "ending_msa"]].copy()
df_demo_join.rename(columns={"ending_msa": "study_club_msas"}, inplace=True)
df_demo_join.dropna(inplace=True)
df_demo_join.drop_duplicates(inplace=True)
df_demo_join = (
    df_demo_join.groupby("customer_internal_id")["study_club_msas"]
    .apply(lambda x: " | ".join(x))
    .reset_index()
)

df_demo = df_demo.merge(df_demo_join, how="left", on="customer_internal_id")
df_demo.head()

# multiple msas
df_demo_join = df_ll[["customer_internal_id", "product", "ending_msa",]].copy()
df_demo_join.rename(columns={"ending_msa": "ending_msas"}, inplace=True)
df_demo_join.dropna(inplace=True)
df_demo_join.drop_duplicates(inplace=True)
df_demo_join = (
    df_demo_join.groupby(["customer_internal_id", "product"])["ending_msas"]
    .apply(lambda x: " | ".join(x))
    .reset_index()
)

df_demo.shape
df_demo = df_demo.merge(
    df_demo_join,
    how="left",
    left_on=["customer_internal_id", "spear_main_product"],
    right_on=["customer_internal_id", "product"],
)
df_demo.rename(columns={"ending_msas": "spear_ending_msas"}, inplace=True)
df_demo.drop(columns=["product"], inplace=True)
df_demo = df_demo.merge(
    df_demo_join,
    how="left",
    left_on=["customer_internal_id", "cerec_main_product"],
    right_on=["customer_internal_id", "product"],
)
df_demo.rename(columns={"ending_msas": "cerec_ending_msas"}, inplace=True)
df_demo.drop(columns=["product"], inplace=True)
df_demo.shape

# multiple paying states
df_demo_join = df_ll[["customer_internal_id", "product", "paying_status",]].copy()
df_demo_join.dropna(inplace=True)
df_demo_join.drop_duplicates(inplace=True)
df_demo_join = (
    df_demo_join.groupby(["customer_internal_id", "product"])["paying_status"]
    .apply(lambda x: " | ".join(x))
    .reset_index()
)

df_demo.shape
df_demo = df_demo.merge(
    df_demo_join,
    how="left",
    left_on=["customer_internal_id", "spear_main_product"],
    right_on=["customer_internal_id", "product"],
)
df_demo.rename(columns={"paying_status": "spear_paying_status"}, inplace=True)
df_demo.drop(columns=["product"], inplace=True)
df_demo = df_demo.merge(
    df_demo_join,
    how="left",
    left_on=["customer_internal_id", "cerec_main_product"],
    right_on=["customer_internal_id", "product"],
)
df_demo.rename(columns={"paying_status": "cerec_paying_status"}, inplace=True)
df_demo.drop(columns=["product"], inplace=True)
df_demo.shape

df_export = df_export.merge(df_demo, how="left", on="customer_internal_id")

# add course history
df_export["refresh_date"] = datetime.date.today()

### add DPMS
df_sps_join = df_sps[
    ["customer_internal_id", "dpms", "demo_scheduled_date", "demo_completion_date"]
].copy()
df_sps_join.drop_duplicates(inplace=True)

sps_dates = ["demo_scheduled_date", "demo_completion_date"]
for d in sps_dates:
    df_sps_join[d] = df_sps_join[d].fillna(datetime.date(1678, 1, 1))
    df_sps_join[d] = pd.to_datetime(df_sps_join[d])

df_sps_join = (
    df_sps_join.groupby(["customer_internal_id", "dpms"])
    .agg({"demo_scheduled_date": "max", "demo_completion_date": "max"})
    .reset_index()
)

for d in sps_dates:
    df_sps_join[d] = np.where(
        df_sps_join[d] < datetime.datetime(1679, 1, 1), pd.NaT, df_sps_join[d]
    )
    df_sps_join[d] = pd.to_datetime(df_sps_join[d])

df_export.shape
df_export = df_export.merge(df_sps_join, how="left", on="customer_internal_id")

#os.remove(r"A:\data\python_data\courses\dropouts.csv")
df_export.to_csv(r"A:\data\python_data\courses\dropouts.csv")

print("Complete")

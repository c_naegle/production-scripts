import psycopg2
import sqlalchemy
import numpy as np
import pandas as pd
import os
import datetime as dt
from keyring import get_password

pd.set_option("display.max_rows", 500)
pd.set_option("display.max_columns", 500)

print("Running...")

database = "spear_edw"
user = "ceops"
password = get_password("ceops", "ceops")
host = "edm-redshift-prod-1.ccvpsxk3s37e.us-east-1.redshift.amazonaws.com"
port = "5439"
schema = "ceops"

connection_string = "redshift+psycopg2://%s:%s@%s:%s/%s" % (
    user,
    password,
    host,
    str(port),
    database,
)
engine = sqlalchemy.create_engine(
    connection_string, connect_args={"options": "-csearch_path={}".format(schema)}
)

df = pd.read_sql_query(
    """
	SELECT internalid,
        assigned_name,
        company_internalid,
        company_name,
        completeddate,
        "contivio call direction",
        "contivio call duration in minutes",
        "contivio call id",
        "contivio call type",
        "contivio contact result",
        "contivio disposition",
        "contivio queue",
        "contivio recording",
        createddate,
        enddate,
        message,
        owner_internalid,
        owner_name,
        phone,
        startdate,
        status,
        title
	FROM phone_call
	WHERE startdate > {}""".format(
        "2020-01-01"
    ),
    engine,
)

df["refresh_date"] = dt.datetime.today()
df["phone"] = df["phone"].str.replace("\\D+", "")

obj_columns = list(df.select_dtypes(include=["object"]).columns.values)
df[obj_columns] = df[obj_columns].replace([None], np.nan)
df["company_internalid"] = df["company_internalid"].astype(float)
df["company_internalid"] = df["company_internalid"].astype("Int32")

df_bob = pd.read_csv(r"A:\data\python_data\sps\platform_to_living_list.csv")

# column formatting: 1. no leading or trailing spaces, lower case, underscores
df_bob.drop(columns=["Unnamed: 0"], inplace=True)
df_bob.columns = df_bob.columns.str.strip().str.lower().str.replace(" ", "_")

# reformat internal id for join
df_bob.rename(columns={"customer_internal_id": "company_internalid"}, inplace=True)
df_bob["company_internalid"] = df_bob["company_internalid"].astype("Int32")

df = df.merge(df_bob, how="left", on="company_internalid")

df.dropna(subset=["office_id", "consultant"], inplace=True)

df["office_id"] = df["office_id"].astype(int)
df["office_name"] = df["office_name"].astype(str)
df[df["office_name"] == "nan"]

df["refresh_date"] = dt.datetime.today()

df.drop(df[df["consultant"] == "Amy Morgan"].index, inplace=True)
df.drop(df[df["consultant"] == "Judy Larocque"].index, inplace=True)

os.remove(r"A:\data\python_data\phone_calls\call_total_sps.csv")
df.to_csv(r"A:\data\python_data\phone_calls\call_total_sps.csv")

print("Complete")

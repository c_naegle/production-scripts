import numpy as np
import pandas as pd
import os
import datetime as dt

print("Running...")

df = pd.read_csv(r"A:\data\python_data\sps\reporting_extracts\sps_time.csv")
df.drop(columns=["Unnamed: 0"], inplace=True)
df.shape
df.columns

df["date"] = pd.to_datetime(df["date"])

covid_guidance = dt.datetime(2020, 3, 16, 0, 0, 0).toordinal()
today = dt.datetime.today().toordinal()
time_past = today - covid_guidance
model_start = time_past + 21

last_week = today - 7
weeks_ago = today - (model_start)
last_saturday = last_week - (last_week % 7) + 6
weeks_sunday = weeks_ago - (weeks_ago % 7)
dt.datetime.fromordinal(last_saturday)
dt.datetime.fromordinal(weeks_sunday)

df = df[df["date"] <= dt.datetime.fromordinal(last_saturday)]
df = df[df["date"] >= dt.datetime.fromordinal(weeks_sunday)]

ada_notice = dt.datetime.strptime("03/16/2020", "%m/%d/%Y")
df["date_index"] = df["date"]

df = df.reset_index().set_index("date_index")

df["covid_flag"] = np.where(df.index < ada_notice, "pre_covid", "post_covid")

df_week = df.groupby(
    [
        "customer",
        "practice_uuid",
        "office_id",
        "office_name",
        "state",
        "team_lead",
        "consultant",
        "contract_start_date",
        "covid_flag",
        pd.Grouper(freq="W"),
    ]
).agg({"production": "sum", "hygiene_production": "sum", "total_collections": "sum"})

df_week = df_week.groupby(
    [
        "customer",
        "practice_uuid",
        "office_id",
        "office_name",
        "state",
        "team_lead",
        "consultant",
        "contract_start_date",
        "covid_flag",
    ]
).agg({"production": "mean", "hygiene_production": "mean", "total_collections": "mean"})

df_week.rename(
    columns={
        "production": "week_production_avg",
        "hygiene_production": "week_hyg_production_avg",
        "total_collections": "week_collections_avg",
    },
    inplace=True,
)

df_week = df_week.unstack("covid_flag")
df_week.columns = ["_".join(col).strip() for col in df_week.columns.values]
df_week.columns

measures_avg = [
    "week_production_avg",
    "week_hyg_production_avg",
    "week_collections_avg",
]

for n in measures_avg:
    df_week["delta_" + n] = (df_week[n + "_post_covid"] / df_week[n + "_pre_covid"]) - 1
    df_week["delta_" + n] = df_week["delta_" + n].round(1)
    df_week["simple_delta_" + n] = df_week["delta_" + n].round(0)
    df_week["simple_delta_" + n] = df_week["simple_delta_" + n].clip(-1, 1)

df_week = df_week.reset_index()
df_week = df_week[
    [
        "customer",
        "practice_uuid",
        "office_id",
        "office_name",
        "state",
        "team_lead",
        "consultant",
        "contract_start_date",
        "delta_week_production_avg",
        "delta_week_hyg_production_avg",
        "delta_week_collections_avg",
        "simple_delta_week_production_avg",
        "simple_delta_week_hyg_production_avg",
        "simple_delta_week_collections_avg",
    ]
]

df_week = df_week.melt(
    id_vars=[
        "customer",
        "practice_uuid",
        "office_id",
        "office_name",
        "state",
        "team_lead",
        "consultant",
        "contract_start_date",
    ],
    value_vars=[
        "delta_week_production_avg",
        "delta_week_hyg_production_avg",
        "delta_week_collections_avg",
        "simple_delta_week_production_avg",
        "simple_delta_week_hyg_production_avg",
        "simple_delta_week_collections_avg",
    ],
    var_name="Averages",
)

measures = ["production", "hygiene_production", "total_collections"]
for n in measures:
    df[n + "_pre_covid"] = np.where(df["covid_flag"] == "pre_covid", df[n], 0)

df_group = df.groupby(
    [
        "customer",
        "practice_uuid",
        "office_id",
        "office_name",
        "state",
        "team_lead",
        "consultant",
        "contract_start_date",
    ]
).agg(
    {
        "production_pre_covid": "sum",
        "hygiene_production_pre_covid": "sum",
        "total_collections_pre_covid": "sum",
    }
)

pre_covid_measures = [
    "production_pre_covid",
    "hygiene_production_pre_covid",
    "total_collections_pre_covid",
]
for n in pre_covid_measures:
    df_group[n] = df_group[n] / 3

os.remove(r"A:\data\python_data\sps\reporting_extracts\spike_model.csv")
df.to_csv(r"A:\data\python_data\sps\reporting_extracts\spike_model.csv")

os.remove(r"A:\data\python_data\sps\reporting_extracts\spike_rank.csv")
df_week.to_csv(r"A:\data\python_data\sps\reporting_extracts\spike_rank.csv")

os.remove(r"A:\data\python_data\sps\reporting_extracts\spike_retro.csv")
df_group.to_csv(r"A:\data\python_data\sps\reporting_extracts\spike_retro.csv")

print("Complete")

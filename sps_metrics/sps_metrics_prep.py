import sqlalchemy
import numpy as np
import pandas as pd
import os
from keyring import get_password

print("Running (Very Long Runtime)...")

user = "cnaegle"
password = get_password("sps_redshift", "cnaegle")
host = "production.cdgx6vphollt.us-west-2.redshift.amazonaws.com"
port = "5439"
database = "sps"
schema = "tgt"

connection_string = "redshift+psycopg2://%s:%s@%s:%s/%s" % (
    user,
    password,
    host,
    str(port),
    database,
)
engine = sqlalchemy.create_engine(
    connection_string, connect_args={"options": "-csearch_path={}".format(schema)}
)

pd.set_option("display.max_rows", 500)
pd.set_option("display.max_columns", 500)

### SUM VALUES
# the metric will come from summing the value in the date range

# query production data
df = pd.read_sql_query(
    """
    SELECT production_detail.practice_uuid,
        office.account_id AS account_id,
        office.id AS office_id,
        office.name AS office_name,
        office.client_enabled AS client_enabled,
        office.practice_management_system_id AS dpms_id,
        office.is_pending AS is_pending,
        office.agreed_to_terms AS agreed_to_terms,
        LEFT(production_detail.procedure_date, 4)::integer AS year,
        production_detail.procedure_date AS date,
        SUM(production_detail.amount) AS production,
        SUM(CASE WHEN users.provider_specialty = 'hygienist' THEN production_detail.amount ELSE 0 END) AS hygiene_production
    FROM production_detail
    LEFT JOIN office ON
        production_detail.practice_uuid = office.practice_uuid
    LEFT JOIN users ON
        production_detail.user_id = users.id
    WHERE production_detail.deleted_at IS NULL
        AND office.deleted_at IS NULL
        AND year > 2015
        AND year < 2021
    GROUP BY production_detail.practice_uuid,
        account_id,
        office.id,
        office_name,
        client_enabled,
        office.practice_management_system_id,
        is_pending,
        agreed_to_terms,
        year,
        date
    HAVING production > 0
    ORDER BY production_detail.practice_uuid,
        date;""",
    engine,
)

# remove test locations, dummy locations, blank locations
fake_offices = [192, 355, 586, 619, 655, 340, 632, 343, 345, 346, 347, 348]
df = df[~df["office_id"].isin(fake_offices)]
df["office_name"] = df["office_name"].str.strip()
df = df[~pd.isnull(df["office_name"])]

# query collection data
df_collection = pd.read_sql_query(
    """
    SELECT practice_uuid,
        LEFT(ar_date, 4)::integer AS year,
        ar_date AS date,
        SUM(insurance_payments) AS ins_payments,
        SUM(patient_payments) AS pat_payments,
        SUM(adjustments) as adjustments,
        SUM(finance_charges) as finance_charges,
        SUM(late_charges) as late_charges
    FROM accounts_receivable_history
    WHERE deleted_at IS NULL
        AND year > 2015
        AND year < 2021
    GROUP BY practice_uuid,
        year,
        date
    ORDER BY practice_uuid,
        year,
        date;""",
    engine,
)

df_collection["date"] = pd.to_datetime(df_collection["date"])

# joining production and collection requires a few moves
# we have to ensure all days can be joined - people work and pay on different days
# our data is from 2016 to 2020, so first generate all dates in 2016 through 2020
all_dates = pd.date_range(start="1/1/2016", end="12/31/2020")
df_dates = pd.DataFrame(data=all_dates, columns=["date"])

# select the practice info and create one row for every practice
df_merge_prep = df[
    [
        "practice_uuid",
        "account_id",
        "office_id",
        "office_name",
        "client_enabled",
        "dpms_id",
        "is_pending",
        "agreed_to_terms",
    ]
].copy()
df_merge_prep.drop_duplicates(inplace=True)

# add location count for each account
df_loc_count = df_merge_prep.groupby("account_id").agg({"office_id": "nunique"})
df_loc_count.rename(columns={"office_id": "location_count"}, inplace=True)
df_merge_prep = df_merge_prep.merge(df_loc_count, on="account_id", how="left")

#df_merge_prep.to_csv(r"C:\working_data\large_sps_tables\metric_test\demographics.csv")

# we're going to outer join on a constant to make one row
# per practice / date pair
df_merge_prep["merge_index"] = 1
df_dates["merge_index"] = 1
df_merge_prep = df_merge_prep.merge(df_dates, on="merge_index", how="outer")
df_merge_prep.drop(columns=["merge_index"], inplace=True)

# join in both datasets
df = df_merge_prep.merge(
    df,
    on=[
        "practice_uuid",
        "account_id",
        "office_id",
        "office_name",
        "client_enabled",
        "dpms_id",
        "is_pending",
        "agreed_to_terms",
        "date",
    ],
    how="left",
)
df = df.merge(df_collection, on=["practice_uuid", "year", "date"], how="left")


### AVG Values
# the metric value will be repeated on every row of the table

# query AR values
df_ar = pd.read_sql_query(
    """
    SELECT 
        practice_uuid,
        sum(balance) as total,
        sum(case when diff <= 30 then balance else 0 end) as current,
        sum(case when diff > 30 and diff <= 60 then balance else 0 end) as thirty,
        sum(case when diff > 60 and diff <= 90 then balance else 0 end) as sixty,
        sum(case when diff > 90 and diff <= 120 then balance else 0 end) as ninety,
        sum(case when diff > 120 then balance else 0 end) as bad_debt
    FROM
        (
            SELECT practice_uuid,
                balance,
                datediff('day', last_procedure_date, CURRENT_DATE) as diff
            FROM accounts_receivable_balances 
            WHERE balance > 0 and 
                deleted_at is null 
        )
    GROUP BY practice_uuid;""",
    engine,
)

# format, join into main table, save for archive
df_ar = df_ar[["practice_uuid", "total"]]
df_ar.rename(columns={"total": "ar_total"}, inplace=True)

df = df.merge(df_ar, on="practice_uuid", how="left")

# query treatment planning values
df_treatment_plan = pd.read_sql_query(
    """
    SELECT practice_uuid,
        COUNT(DISTINCT CASE WHEN is_new = TRUE AND planned = TRUE THEN patient_id ELSE NULL END) AS new_planned_pts,
        COUNT(DISTINCT CASE WHEN (is_new = TRUE AND scheduled_date IS NOT NULL AND accepted = TRUE) THEN patient_id ELSE NULL END) AS new_scheduled_pts,
        COUNT(DISTINCT CASE WHEN (is_new = TRUE AND completed_date IS NOT NULL AND completed = TRUE) THEN patient_id ELSE NULL END) AS new_completed_pts,
        SUM(CASE WHEN is_new = TRUE AND planned = TRUE THEN amount ELSE NULL END) as new_planned_opportunity,
        SUM(CASE WHEN (scheduled_date IS NOT NULL AND is_new = TRUE AND accepted = TRUE) THEN amount ELSE 0 END) as new_scheduled_amt,
        SUM(CASE WHEN (completed_date IS NOT NULL AND is_new = TRUE AND completed = TRUE) THEN amount ELSE 0 END) as new_completed_amt,
        COUNT(DISTINCT (CASE WHEN is_new = TRUE AND planned = TRUE THEN NULL ELSE patient_id END)) AS existing_planned_pts,
        COUNT(DISTINCT (CASE WHEN (scheduled_date IS NOT NULL AND accepted = TRUE AND (is_new = FALSE OR is_new IS NULL)) THEN patient_id ELSE NULL END)) AS existing_scheduled_pts,
        COUNT(DISTINCT (CASE WHEN (completed_date IS NOT NULL AND completed = TRUE AND (is_new = FALSE OR is_new IS NULL)) THEN patient_id ELSE NULL END)) AS existing_completed_pts,
        SUM(CASE WHEN (is_new = FALSE OR is_new IS NULL) AND planned = TRUE THEN amount ELSE NULL END) as existing_planned_opportunity,
        SUM(CASE WHEN (scheduled_date IS NOT NULL AND accepted = TRUE AND (is_new = FALSE OR is_new IS NULL)) THEN amount ELSE 0 END) as existing_scheduled_amt,
        SUM(CASE WHEN (completed_date IS NOT NULL AND completed = TRUE AND (is_new = FALSE OR is_new IS NULL)) THEN amount ELSE 0 END) as existing_completed_amt
    FROM treatment_planning_efficiency
    WHERE days = '365'
        AND amount > 0
    GROUP BY practice_uuid
    ORDER BY practice_uuid;""",
    engine,
)

# join into main table, save for archive
df = df.merge(df_treatment_plan, on="practice_uuid", how="left")

# query patient base
df_patients_current = pd.read_sql_query(
    """
    SELECT practice_uuid,
        COUNT(DISTINCT patient_id) AS patient_base
    FROM flattened_patient_base
    WHERE active_status = 1
        AND DATEDIFF(month, last_appointment, CURRENT_DATE) < 19
    GROUP BY practice_uuid;""",
    engine,
)

# join into main table, save for archive
df = df.merge(df_patients_current, on="practice_uuid", how="left")

# query 6 month historic patient base
df_patients_6_mo_historic = pd.read_sql_query(
    """
    SELECT practice_uuid,
        COUNT(DISTINCT patient_id) AS patient_base_6_mo
    FROM flattened_patient_base
    WHERE active_status = 1
        AND DATEDIFF(month, last_appointment, CURRENT_DATE) < 25
        AND DATEDIFF(month, last_appointment, CURRENT_DATE) > 6
    GROUP BY practice_uuid;""",
    engine,
)

# join into main table, save for archive
df = df.merge(df_patients_6_mo_historic, on="practice_uuid", how="left")

# query doctor count
df_team = pd.read_sql_query(
    """
    SELECT production_detail.practice_uuid,
        production_detail.user_id,
        users.name AS user_name,
        users.provider_specialty
    FROM production_detail
    LEFT JOIN users ON
        production_detail.user_id = users.id
    LEFT JOIN office ON
        production_detail.practice_uuid = office.practice_uuid    
    WHERE production_detail.procedure_date BETWEEN '2019-10-01' AND '2019-12-31'
        AND production_detail.deleted_at IS NULL
    """,
    engine,
)

df_team.columns

# prep user counts
df_team["doctors"] = np.where(
    df_team["provider_specialty"] == "doctor", df_team["user_id"], np.nan
)
df_team["doctors"] = np.where(
    df_team["provider_specialty"] == "periodontist",
    df_team["user_id"],
    df_team["doctors"],
)
df_team["team"] = np.where(df_team["doctors"].isnull(), df_team["user_id"], np.nan)

# sum user counts
df_team_counts = df_team.groupby("practice_uuid").agg(
    {"team": "nunique", "doctors": "nunique"}
)

df_team_counts.rename(
    columns={"team": "team_count", "doctors": "doctor_count"}, inplace=True
)

# join into main table, save for archive
df = df.merge(df_team_counts, on="practice_uuid", how="left")

'''
df_cdt = pd.read_sql_query("""
    SELECT production_detail.practice_uuid,
        office.id AS office_id,
        office.name AS office_name,
        production_detail.cdt_code,
        COUNT(production_detail.cdt_code) AS count
    FROM production_detail
    LEFT JOIN users ON
        production_detail.user_id = users.id
    LEFT JOIN office ON
        production_detail.practice_uuid = office.practice_uuid    
    WHERE production_detail.procedure_date BETWEEN '2018-12-11' AND '2019-12-11'
        AND production_detail.deleted_at IS NULL
        AND office_id = 695
    GROUP BY production_detail.practice_uuid,
        office_id,
        office_name,
        production_detail.cdt_code
    ORDER BY production_detail.practice_uuid,
        office_id,
        office_name,
        production_detail.cdt_code
    """, engine)
print(df_cdt.head())
df_cdt.to_csv(r'C:\working_data\csv_exports\695_cdt_year_test.csv')
'''

### add in employee and team info
df_sps_team = pd.read_csv(r"A:\data\python_data\sps\platform_to_living_list.csv",)
df_sps_team.drop(columns=["Unnamed: 0", "office_name"], inplace=True)

# merging in team data
df = df.merge(df_sps_team, on="office_id", how="left")

# output prepped data
df.to_csv(r"A:\data\python_data\sps\sps_metrics_new.csv")
df.columns
df.shape


### treatment planning testing
'''
df_treatment_plan = pd.read_sql_query(
    """
    SELECT office.id AS office_id,
        patient_id,
        cdt_code,
        planned_date,
        scheduled_date,
        completed_date
    FROM treatment_planning_efficiency
    LEFT JOIN office ON
        treatment_planning_efficiency.practice_uuid = office.practice_uuid
    WHERE days = 'YTD'
        AND ((scheduled_date >= '2020-01-01' AND scheduled_date <= '2020-12-31')
            OR (completed_date >= '2020-01-01' AND completed_date <= '2020-05-05'))
    GROUP BY office_id,
        patient_id,
        cdt_code,
        planned_date,
        scheduled_date,
        completed_date
    """,
    engine,
)

print(df_treatment_plan.shape)
print(df_treatment_plan.columns)

# step 1, concatenate all d-codes
# step 2, look for only D0 and D1 left 2 char
# step 3, flag all sched code rows with TRUE in step 2 as "hygiene"
# step 4, drop d-codes, deduplicate
# Conclusion - should yeild all hygiene/routine and all non

df = df_treatment_plan.groupby(
    ["office_id", "patient_id", "planned_date", "scheduled_date", "completed_date"]
)["cdt_code"].apply(list)

df = df.reset_index()
df.shape
df.columns

hyg_prev_codes = [
    "D0120",
    "D0140",
    "D0145",
    "D0150",
    "D0160",
    "D0170",
    "D0171",
    "D0180",
    "D0190",
    "D0191",
    "D0210",
    "D0220",
    "D0230",
    "D0240",
    "D0250",
    "D0251",
    "D0270",
    "D0272",
    "D0273",
    "D0274",
    "D0277",
    "D0310",
    "D0320",
    "D0321",
    "D0322",
    "D0330",
    "D0340",
    "D0350",
    "D0351",
    "D0364",
    "D0365",
    "D0366",
    "D0367",
    "D0368",
    "D0369",
    "D0370",
    "D0371",
    "D0380",
    "D0381",
    "D0382",
    "D0383",
    "D0384",
    "D0385",
    "D0386",
    "D0391",
    "D0393",
    "D0394",
    "D0395",
    "D0411",
    "D0412",
    "D0414",
    "D0415",
    "D0416",
    "D0417",
    "D0418",
    "D0419",
    "D0422",
    "D0423",
    "D0425",
    "D0431",
    "D0460",
    "D0470",
    "D0472",
    "D0473",
    "D0474",
    "D0475",
    "D0476",
    "D0477",
    "D0478",
    "D0479",
    "D0480",
    "D0481",
    "D0482",
    "D0483",
    "D0484",
    "D0485",
    "D0486",
    "D0502",
    "D0600",
    "D0601",
    "D0602",
    "D0603",
    "D0999",
    "D1110",
    "D1120",
    "D1206",
    "D1208",
    "D1310",
    "D1320",
    "D1330",
    "D1351",
    "D1352",
    "D1353",
    "D1354",
    "D1510",
    "D1516",
    "D1517",
    "D1520",
    "D1526",
    "D1527",
    "D1550",
    "D1551",
    "D1552",
    "D1553",
    "D1555",
    "D1556",
    "D1557",
    "D1558",
    "D1575",
    "D1999",
]

df["hyg_prev"] = np.where(
    df["cdt_code"].apply(lambda x: set(x).issubset(hyg_prev_codes)),
    "Hygiene/Prevent",
    "Other",
)
df.head()

df_demo = pd.read_csv(r"C:\working_scripts\sps_analytics\platform_to_living_list.csv")
df_demo.drop(columns=["Unnamed: 0"], inplace=True)
df_demo.dtypes
df_treatment_plan.dtypes

df_demo.dropna(subset=["office_id"], inplace=True)
df_demo["office_id"] = df_demo["office_id"].astype(int)
df = df.merge(df_demo, how="left", on="office_id")
df.dropna(subset=["office_name"], inplace=True)
df.head()

eight_weeks_pre_covid = "2020-01-20"
covid_start = "2020-03-16"
today = "2020-05-05"
eight_weeks_from_today = "2020-06-30"

df["pre_covid"] = np.where(((df["scheduled_date"] >= eight_weeks_pre_covid)
    & (df["scheduled_date"] < covid_start)), 1, 0)
df["covid"] = np.where(((df["scheduled_date"] >= covid_start)
    & (df["scheduled_date"] < today)), 1, 0)
df["post_covid"] = np.where(((df["scheduled_date"] >= today)
    & (df["scheduled_date"] < eight_weeks_from_today)), 1, 0)

df.columns

df.to_csv(r"C:\working_data\2020_scheduled.csv")
'''

import datetime

df = pd.read_sql_query(
    """
    SELECT office.id AS office_id,
        patient_id,
        user_id,
        appointment_id,
        appointment_date,
        appointment_status_id, 
        appt_created_at,
        is_hygiene,
        patient_appointment.deleted_at,
        SUM(amount)
    FROM patient_appointment
    LEFT JOIN office ON
        patient_appointment.practice_uuid = office.practice_uuid
    WHERE appointment_date >= '2020-01-01'
        AND appointment_date <= '2020-12-31'
        AND office.deleted_at IS NULL
    GROUP BY office_id,
        patient_id,
        user_id,
        appointment_id,
        appointment_date,
        appointment_status_id, 
        appt_created_at,
        is_hygiene,
        patient_appointment.deleted_at
    """,
    engine,
)
# AND patient_appointment.deleted_at IS NULL

df_demo = pd.read_csv(r"A:\data\python_data\sps\platform_to_living_list.csv")
df_demo.drop(columns=["Unnamed: 0"], inplace=True)
df_demo.dtypes
df.dtypes

df_demo.dropna(subset=["office_id"], inplace=True)
df_demo["office_id"] = df_demo["office_id"].astype(int)
df = df.merge(df_demo, how="left", on="office_id")
df.dropna(subset=["office_name"], inplace=True)
df.head()

eight_weeks_pre_covid = "2020-01-20"
covid_start = "2020-03-16"
today = "2020-05-10"
eight_weeks_from_today = "2020-07-05"

df["COVID_Date_Categories"] = np.where(
    (
        (df["appointment_date"] >= eight_weeks_pre_covid)
        & (df["appointment_date"] < covid_start)
    ),
    "Pre-COVID 8 Weeks",
    0,
)
df["COVID_Date_Categories"] = np.where(
    ((df["appointment_date"] >= covid_start) & (df["appointment_date"] < today)),
    "COVID Period",
    df["COVID_Date_Categories"],
)
df["COVID_Date_Categories"] = np.where(
    (
        (df["appointment_date"] >= today)
        & (df["appointment_date"] < eight_weeks_from_today)
    ),
    "Post-COVID 8 Weeks",
    df["COVID_Date_Categories"],
)

df.rename(columns={"state": "state_code"}, inplace=True)
df["refresh_date"] = datetime.date.today()
df_regions = pd.read_csv(r"A:\data\python_data\sps\state_regions.csv")
df = df.merge(df_regions, how="left", on="state_code")

os.remove(r"A:\data\python_data\sps\reporting_extracts\sps_appointments_snapshot_v4.csv")
df.to_csv(
    r"A:\data\python_data\sps\reporting_extracts\sps_appointments_snapshot_v4.csv"
)

print("Complete")

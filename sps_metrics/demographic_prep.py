#%% [markdown]

# # Joining SPS Living List to SPS Platform Data

# ### author: cnaegle
# ### written: 2/4/2020
# ### refactored: 2/26/2020

# ### purpose
# This script ties customers from netsuite to the platform.

# We'll start with sps living list, and make some formatting changes.

#%%
import sqlalchemy
import numpy as np
import pandas as pd
import os
from keyring import get_password

print("Running...")

# connection string variables
database = "spear_edw"
user = "ceops"
password = get_password("ceops", "ceops")
host = "edm-redshift-prod-1.ccvpsxk3s37e.us-east-1.redshift.amazonaws.com"
port = "5439"
schema = "ceops"

pd.set_option("display.max_rows", 500)
pd.set_option("display.max_columns", 500)

# connect to Redshift and pull entire sps living list
connection_string = "redshift+psycopg2://%s:%s@%s:%s/%s" % (
    user,
    password,
    host,
    str(port),
    database,
)
engine = sqlalchemy.create_engine(
    connection_string, connect_args={"options": "-csearch_path={}".format(schema)}
)
df = pd.read_sql_query(
    """
    SELECT * FROM living_list_native
    WHERE department = 'Spear Practice Solutions'
    """,
    engine,
)

df.columns
df.shape
df.groupby("item")["customer"].nunique()

# flag sps lite
df["sps_product_category"] = np.where(
    df["item"] == "Spear Subscription Items (CB) : Pride WITHOUT Analytics",
    "SPS Lite",
    "SPS",
)
df["sps_product_category"] = np.where(
    df["item"] == "Spear Subscription Items (CB) : Pride WITH Analytics",
    "SPS Lite",
    df["sps_product_category"],
)

# clean symbols from columns in ll_clean_columns
def string_clean(string_column):
    return string_column.str.replace(
        "[.,\/#!$%\^&\*;:{}=\-_`~() ']", "", regex=True
    ).str.lower()


df["client_name"] = df["first_name"] + df["last_name"]

ll_clean_columns = [
    "client_name",
    "sps_practice_name",
    "address_1",
    "city",
    "phone",
]
for n in ll_clean_columns:
    df[n] = string_clean(df[n])

# standardize phone and zip lengths, add state to company name
# prepares for practices with the same name
df["phone"] = df["phone"].str[-10:]
df["zip"] = df["zip"].str[:5]
df["netsuite_practice_name"] = df["sps_practice_name"]
df["company_state"] = df["sps_practice_name"] + "|" + df["state"]
df.shape
df.columns
# %% [markdown]
### Platform Data
# At this point we'll bring platform data to match to the living list.

#%%
user = "cnaegle"
password = get_password("sps_redshift", "cnaegle")
host = "production.cdgx6vphollt.us-west-2.redshift.amazonaws.com"
port = "5439"
database = "sps"
schema = "tgt"

connection_string = "redshift+psycopg2://%s:%s@%s:%s/%s" % (
    user,
    password,
    host,
    str(port),
    database,
)
engine = sqlalchemy.create_engine(
    connection_string, connect_args={"options": "-csearch_path={}".format(schema)}
)
df_office = pd.read_sql_query(
    """
    SELECT *
    FROM office
    WHERE deleted_at IS NULL""",
    engine,
)

df_office.shape
df_office.columns

# fake offices identified manually
fake_offices = [192, 355, 586, 619, 655, 340, 632, 343, 345, 346, 347, 348]
df_office = df_office[~df_office["id"].isin(fake_offices)]

df_office["office_name"] = df_office["name"]
office_clean_columns = ["name", "address1", "city"]
for n in office_clean_columns:
    df_office[n] = string_clean(df_office[n])

df_office.rename(columns={"name": "sps_practice_name"}, inplace=True)
df_office.rename(columns={"id": "office_id"}, inplace=True)

df_office["company_state"] = df_office["sps_practice_name"] + "|" + df_office["state"]
df_office = df_office[
    ["company_state", "office_id", "office_name", "practice_management_system_id"]
]

df = df.merge(
    df_office[pd.notnull(df_office["company_state"])], how="left", on="company_state"
)

#%% [markdown]
# ### Manual Edits to Improve Join
# A series of manual checks were made to improve reporting quality while edits
# are made to Netsuite. First the columns are trimmed.
#%%
# trim and correct a few blanks
df.columns
df = df[
    [
        "sales_order_number",
        "sps_product_category",
        "item",
        "renewal_status",
        "active_not_active",
        "contract_start_date",
        "contract_end_date",
        "cancellation_date",
        "customer",
        "customer_internal_id",
        "phone",
        "email",
        "first_name",
        "last_name",
        "address_1",
        "address_2",
        "city",
        "state",
        "zip",
        "country",
        "office_name",
        "netsuite_practice_name",
        "sps_practice_name",
        "ending_msa",
        "office_id",
        "practice_management_system_id",
        "company_state",
    ]
]

# manual fixes for name mismatches
# TODO: look for talordentistry|FL to show 750 soon 2/26/2020
company_name_corrections = {
    "jamestmaholickdmdpc": 690,
    "reflectionsdentalcare": 368,
    "coloradodentalarts": 188,
    "joycollierdesertvistawest": 10,
    "tlcdentistryfortworth": 643,
    "thompsonfamilydental": 745,
    "bryanjshanahanddspcflagstaff": 491,
    "bryanjshanahanddspccottonwoodlocation": 591,
    "bryanjshanahanddspcsedonalocation": 592,
    "princetoncenterfordentalaestheticsandimplants": 47,
    "drsbowmanpadgettassociates": 679,
    "larrybowersdds": 551,
    "dd4k": 711,
    "aunondentalgroup": 225,
    "cdadunedin": 657,
    "arizonafamilydental": 729,
    "signaturedentistryofarvada": 707,
    "porteousburkefamilydentistry": 587,
    "williamsportsmiles": 598,
    "monahanfamilycosmeticdentisty": 702,
    "libertydentalplc": 450,
    "signaturesmilesllc": 609,
    "tlcdentistryplano": 644,
    "cordatafamilydentist": 227,
    "coeurdâ€™alenedentalcenter": 613,
    "gumdentalassociates": 331,
    "hearndentistrykokomolocation": 718,
    "hearndentistrynoblesvillelocation": 725,
    "grandviewfamilycosmeticdentistry": 534,
    "hutchensfamilydentistry": 649,
    "soltfamilydental": 637,
    "pelhamlinksfamilydentistry": 673,
    "anaamayaddsms": 9999,  # don't know
    "hooksettfamilydental": 9998,  # don't know
    "countrysideoaksdental": 9997,  # don't know
    "countrysideoaksdental": 9996,  # don't know
    "griffithdentalcare": 9995,  # don't know
    "smiledental": 9994,  # don't know
    "calcagnofamilydentistry": 9992,  # don't know
}

# it's important that these customers only have one contract
customer_name_corrections = {
    "9464 Tom Northway": 154,
    "10481 Bruce Barker": 332,
    "1744233652 Mario DeNicola": 710,  ### TODO: double check on this, assign name
}
### freaky : familydentalcareofcoronasusanevansfamilydentalhealth, tom northway
###     calcagnofamilydentistry, 1744256517 Muna Abudayyeh

df.drop(
    df[
        df["sps_practice_name"]
        == "familydentalcareofcoronasusanevansfamilydentalhealth"
    ].index,
    inplace=True,
)

df.drop(
    df[df["contract_start_date"] == pd.NaT].index, inplace=True,
)
df.shape
df.columns

# general formula to handle corrections
def manual_corrections(corrections, condition_column, fix_column):
    for n in corrections:
        df[fix_column] = np.where(
            df[condition_column] == n, corrections[n], df[fix_column]
        )


manual_corrections(company_name_corrections, "sps_practice_name", "office_id")
manual_corrections(customer_name_corrections, "customer", "office_id")
df.drop_duplicates(inplace=True)
df.shape

#%% [markdown]
# ### Bring in HR Table
# This table will control who appears as team lead for each consultant. Join
# is handled by constructing an email for each consultant and manually fixing
# the few that break the pattern.

# %%
# import hr table, format/rename columns and keep the useful ones
df_hr = pd.read_excel(r"A:\data\excel\HR_data_4.30.2020.xlsx")
df_hr.columns = df_hr.columns.str.lower().str.strip().str.replace(" ", "_")
df_hr = df_hr[["self_service_email", "employee_name", "manager"]]
df_hr.rename(
    columns={"manager": "team_lead", "self_service_email": "consultant_email"},
    inplace=True,
)

# generate emails in main data to prepare for join
df.rename(columns={"ending_msa": "consultant"}, inplace=True)
df["consultant_email"] = df["consultant"].str[0].str.lower()
df["last_name"] = df["consultant"].str.split(" ").str[-1].str.lower()
df["consultant_email"] = (
    df["consultant_email"] + df["last_name"] + "@speareducation.com"
)

# manual fixes for unusual emails
consultant_email_corrections = {
    "mwheaton@speareducation.com": "mlwheaton@speareducation.com",
    "mmullett@speareducation.com": "dmullett@speareducation.com",
}
manual_corrections(consultant_email_corrections, "consultant_email", "consultant_email")

# merge and delete duplicates
df = df.merge(df_hr, how="left", on="consultant_email")
df.drop_duplicates(inplace=True)
df.columns
df.shape

# %% [markdown]
# ### Prepare export for use in dashboards
# We'll drop unneeded columns, rename contract columns to reflect that there
# are multiple contracts merged into the range, and merge the office_ids into
# the deduplicated list.

# %%
# select active, save a list of customer/office/consultant sets
df_team = df[df["active_not_active"] == "Active"].copy()
df_team["office_name"] = np.where(
    pd.isna(df_team["office_name"]),
    df_team["netsuite_practice_name"],
    df_team["office_name"],
)

df_team = df_team[
    [
        "customer",
        "customer_internal_id",
        "sps_product_category",
        "office_id",
        "office_name",
        "company_state",
        "practice_management_system_id",
        "email",
        "state",
        "zip",
        "country",
        "team_lead",
        "consultant",
        "active_not_active",
    ]
]
df_team.drop_duplicates(inplace=True)

# merge all contract ranges together, rename to reflect multiple contracts
df_stats = df.groupby(["customer", "customer_internal_id", "office_id"]).agg(
    {"contract_start_date": "min", "contract_end_date": "max",}
)
df_stats.rename(
    columns={
        "contract_start_date": "contracts_start",
        "contract_end_date": "contracts_end",
    },
    inplace=True,
)

# merge contract ranges in
# df_team.drop(columns=[])
df_team = df_team.merge(
    df_stats, how="left", on=["customer", "customer_internal_id", "office_id"]
)
df_team.shape

df_team[pd.isna(df_team["office_name"])]

# label DPMS
dpms_map = {
    1: "Dentrix G5",
    2: "Dentrix G6",
    3: "Dentrix G6.1",
    4: "Dentrix G6.2",
    5: "Eaglesoft 17",
    6: "Eaglesoft 18",
    7: "OpenDental 16.4",
    8: "Eaglesoft 19",
    9: "Eaglesoft 20",
    10: "Dentrix G7",
}

df_team["DPMS"] = df_team["practice_management_system_id"].map(dpms_map)

# export
os.remove(r"A:\data\python_data\sps\platform_to_living_list.csv")
df_team.to_csv(r"A:\data\python_data\sps\platform_to_living_list.csv")

print("Complete")

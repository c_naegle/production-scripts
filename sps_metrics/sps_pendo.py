import numpy as np
import pandas as pd
import os
import datetime as dt

pd.set_option("display.max_rows", 500)
pd.set_option("display.max_columns", 500)

print("Running...")

# pendo activity data
df = pd.read_csv(r"A:\data\csv\pendo_SPS_visitors_30_days.csv")

df.columns = df.columns.str.replace("[.,\/#!$%\^&\*;:{}=\-_`~()']", "", regex=True)
df.columns = df.columns.str.lower()
df.columns = df.columns.str.replace(" ", "_")
df.columns = df.columns.str.replace("_all_apps", "")

df.rename(
    columns={
        "practicename": "practice_name",
        "email": "pendo_email",
        "state": "pendo_state",
        "zip": "pendo_zip",
    },
    inplace=True,
)
df.columns

df.shape
df = df.drop_duplicates(
    subset=[
        "visitor_id",
        "visitor_name",
        "role",
        "pendo_email",
        "city",
        "pendo_state",
        "pendo_zip",
    ],
    keep="first",
)
df.shape

# sps metrics demo
df_sps = pd.read_csv(r"A:\data\python_data\sps\platform_to_living_list.csv",)
df_sps.drop(columns=["Unnamed: 0"], inplace=True)
df_sps.shape
df_sps.columns

# pendo to sps metrics join
os.remove(r"A:\data\python_data\courses\dropouts.csv")
df_demo = pd.read_csv(r"A:\data\csv\pendo_SPS_account_info.csv")

df_demo.columns = df_demo.columns.str.replace(
    "[.,\/#!$%\^&\*;:{}=\-_`~()']", "", regex=True
)
df_demo.columns = df_demo.columns.str.lower()
df_demo.columns = df_demo.columns.str.replace(" ", "_")
df_demo.columns = df_demo.columns.str.replace("_all_apps", "")

df_demo.shape
df_demo.columns

df_demo = df_demo[["email", "account_id"]]
df_demo.rename(
    columns={"email": "pendo_email", "account_id": "office_id"}, inplace=True
)

df_sps = df_sps.merge(df_demo, on="office_id", how="left")
df_sps = df_sps.merge(df, on="pendo_email")

df_sps["owner_flag"] = np.where(df_sps["pendo_email"] == df_sps["email"], 1, np.nan)
df_sps["owner_flag"].sum()

os.remove(r"A:\data\python_data\sps\reporting_extracts\sps_pendo.csv")
df_sps.to_csv(r"A:\data\python_data\sps\reporting_extracts\sps_pendo.csv")

print("Complete")

import numpy as np
import pandas as pd
import os
import datetime as dt

print("Running...")

df = pd.read_csv(
    r"A:\data\python_data\sps\sps_metrics_with_ch.csv", low_memory=False,
)
df.drop(columns=["Unnamed: 0"], inplace=True)
df.columns
df.shape

df.rename(
    columns={
        "contracts_start": "contract_start_date",
        "contracts_end": "contract_end_date",
    },
    inplace=True,
)

df.groupby("year")["office_id"].nunique()

"""
source columns:
    ['practice_uuid', 'account_id', 'office_id', 'office_name', 'state',   
    'client_enabled', 'dpms_id', 'is_pending', 'agreed_to_terms',
    'location_count', 'date', 'year', 'production', 'hygiene_production', 
    'ins_payments', 'pat_payments', 'adjustments', 'finance_charges',     
    'late_charges', 'ar_total', 'new_planned_pts', 'new_scheduled_pts',   
    'new_completed_pts', 'new_planned_opportunity', 'new_scheduled_amt',  
    'new_completed_amt', 'existing_planned_pts', 'existing_scheduled_pts',
    'existing_completed_pts', 'existing_planned_opportunity',
    'existing_scheduled_amt', 'existing_completed_amt', 'patient_base',   
    'patient_base_6_mo', 'team_count', 'doctor_count', 'customer',        
    'customer_internal_id', 'sps_product_category', 'company_state',      
    'team_lead', 'consultant', 'contract_start_date', 'contract_end_date',
    'ch_team_call', 'ch_leadership_call', 'ch_attended_poe_dates',        
    'ch_attended_course_dates', 'ch_scheduled_course_dates',
    'ch_course_attendance', 'ch_non_sps_campus_events']

client profile:
    6cccccc
    position 1: number of doctors (numeric)
    position 2: team size (alpha)
    position 3: revenue (alpha)
    position 4: patient base (alpha)
    position 5: location count (alpha)
"""

### guide

### kpi calc
# 1.    6 mo production / year ago 6 mo period
# 2.    6 mo collections / year ago 6 mo period
# 3.    12 mo collections efficiency
#           (total_collections - 12_mo_adj_prod(prod - adjustments))
# 4.    hygiene production 30 days / prod 30 days
# 5.    hygiene production 365 days / prod 365
# 6.    AR % of avg mo prod 12 mo (curr AR / (12_mo_prod / 12))
# 7.    accepted new pt. rev (new sched amt / new sched pts)
# 8.    accepted exist pt. rev (exist sched amt / exist sched pts)
# 9.    new patient completion %
# 10.   exist patient completion %

### kpi and client profile ranges
kpi_ranges = {
    "kpi_12_mo_production": {1: -0.1, 2: 0.0 - 0.05},
    "kpi_12_mo_collection": {1: -0.12, 2: -0.02},
    "kpi_12_mo_collection_efficiency": {1: 0.95, 2: 0.98},
    "kpi_30_day_hygiene_production": {1: 0.25, 2: 0.30},
    "kpi_12_mo_hygiene_production": {1: 0.25, 2: 0.30},
    "kpi_AR_average_month_production": {1: 1.25, 2: 1.50},
    "kpi_new_pts_avg_revenue": {1: 1000, 2: 1999},
    "kpi_existing_pts_avg_revenue": {1: 1000, 2: 1999},
    "kpi_new_pts_completion": {1: 0.88, 2: 0.95},
    "kpi_existing_pts_completion": {1: 0.88, 2: 0.95},
}

kpi_weighting = {
    "kpi_12_mo_production": 0.25,
    "kpi_12_mo_collection": 0.125,
    "kpi_12_mo_collection_efficiency": 0.125,
    "kpi_30_day_hygiene_production": 0.1,
    "kpi_12_mo_hygiene_production": 0.1,
    "kpi_AR_average_month_production": 0.05,
    "kpi_new_pts_avg_revenue": 0.0625,
    "kpi_existing_pts_avg_revenue": 0.0625,
    "kpi_new_pts_completion": 0.0625,
    "kpi_existing_pts_completion": 0.0625,
}

kpi_category = {
    "kpi_6_mo_production": "production",
    "kpi_6_mo_collection": "production",
    "kpi_12_mo_collection_efficiency": "production",
    "kpi_30_day_hygiene_production": "hygiene",
    "kpi_12_mo_hygiene_production": "hygiene",
    "kpi_new_pts_avg_revenue": "case_acceptance",
    "kpi_existing_pts_avg_revenue": "case_acceptance",
    "kpi_new_pts_completion": "case_acceptance",
    "kpi_existing_pts_completion": "case_acceptance",
}

ch_ranges = {
    "ch_attended_poe_dates": 1,
    "ch_course_attendance": 1,
}

ch_weighting = {
    "ch_team_call": 0.4,
    "ch_leadership_call": 0.3,
    "ch_attended_poe_dates": 0.1,
    "ch_course_attendance": 0.2,
}

profile_columns = ["team_count", "12_mo_production", "patient_base", "location_count"]
profile_ranges = {
    "team_count": {26: "e", 21: "d", 16: "c", 12: "b", 8: "a"},
    "12_mo_production": {
        5500001: "e",
        4500001: "d",
        3500001: "c",
        2500001: "b",
        1000001: "a",
    },
    "patient_base": {5001: "e", 4001: "d", 3001: "c", 2001: "b", 1001: "a"},
    "location_count": {8: "e", 6: "d", 4: "c", 3: "b", 2: "a"},
}

### TODO:
### build out $$$ 6 metrics

## FORMATTING
# column formatting: 1. no leading or trailing spaces, lower case, underscores
df.columns = df.columns.str.strip().str.lower().str.replace(" ", "_")

# format date, calculate days from the end of last month
# round to days ('d'), convert to integer
df["date"] = pd.to_datetime(df["date"])

from pandas.tseries.offsets import MonthEnd

month_close = dt.datetime.today() + MonthEnd(-1)

df = df[df["date"] > "12-31-2016"]
# df["this_month"] = np.where(df["date"] > month_close, 1, 0)

df["past_days"] = month_close - df["date"]
df["past_days"] = df["past_days"].dt.round("d")
df["past_days"] = df["past_days"].dt.days.astype("int32")

# create total collections
df["total_collections"] = (
    df["pat_payments"] * (-1)
    + df["ins_payments"] * (-1)
    + df["late_charges"]
    + df["finance_charges"]
)

# preparing time ranges on $ metrics, a column for all sums needed for kpis
# very repetitive, but beneficial for folks new to pandas, I think
month_days = 31
half_year_days = 183
year_days = 366
year_and_half_days = 548
two_year_days = 732

df["30_day_production"] = np.where(df["past_days"] < month_days, df["production"], 0)

df["30_day_hygiene_production"] = np.where(
    df["past_days"] < month_days, df["hygiene_production"], 0
)

df["12_mo_production"] = np.where(df["past_days"] < year_days, df["production"], 0)

df["previous_12_mo_production"] = np.where(
    (df["past_days"] >= year_days) & (df["past_days"] < two_year_days),
    df["production"],
    0,
)

df["12_mo_hygiene_production"] = np.where(
    df["past_days"] < year_days, df["hygiene_production"], 0
)

df["12_mo_collection"] = np.where(
    df["past_days"] < year_days, df["total_collections"], 0
)

df["previous_12_mo_collection"] = np.where(
    (df["past_days"] >= year_days) & (df["past_days"] < two_year_days),
    df["total_collections"],
    0,
)

df["12_mo_adjustments"] = np.where(df["past_days"] < year_days, df["adjustments"], 0)

# aggregate all practices to one row per practice
# the groupby function doesn't like N/As, and will drop them by default
# we save the demographic/info columns for later
# we only pass practice_uuid to avoid drops - groupby will drop rows with nulls
df_demo = df[
    [
        "practice_uuid",
        "account_id",
        "office_id",
        "office_name",
        "customer",
        "state",
        "client_enabled",
        "dpms_id",
        "contract_start_date",
        "contract_end_date",
        "is_pending",
        "agreed_to_terms",
        "location_count",
        "team_count",
        "doctor_count",
        "consultant",
        "team_lead",
    ]
].copy()
df_demo.drop_duplicates(inplace=True)

# select time data columns, keep 2018 and on
df_time_data = df[
    [
        "customer",
        "practice_uuid",
        "office_id",
        "office_name",
        "state",
        "team_lead",
        "consultant",
        "contract_start_date",
        "date",
        "production",
        "hygiene_production",
        "total_collections",
    ]
].copy()

df_kpi = (
    df.groupby(["practice_uuid"])
    .agg(
        {
            "30_day_production": "sum",
            "30_day_hygiene_production": "sum",
            "12_mo_production": "sum",
            "previous_12_mo_production": "sum",
            "12_mo_hygiene_production": "sum",
            "12_mo_collection": "sum",
            "previous_12_mo_collection": "sum",
            "12_mo_adjustments": "sum",
            "ar_total": "mean",
            "new_planned_opportunity": "mean",
            "existing_planned_opportunity": "mean",
            "new_scheduled_amt": "mean",
            "existing_scheduled_amt": "mean",
            "new_completed_amt": "mean",
            "existing_completed_amt": "mean",
            "new_planned_pts": "mean",
            "new_scheduled_pts": "mean",
            "new_completed_pts": "mean",
            "existing_planned_pts": "mean",
            "existing_scheduled_pts": "mean",
            "existing_completed_pts": "mean",
            "patient_base": "mean",
            "ch_team_call": "mean",
            "ch_leadership_call": "mean",
            "ch_attended_poe_dates": "mean",
            "ch_attended_course_dates": "mean",
            "ch_scheduled_course_dates": "mean",
            "ch_course_attendance": "mean",
            "ch_non_sps_campus_events": "mean",
        }
    )
    .reset_index()
)
df_kpi.shape

# check to see if we have all practices
df["practice_uuid"].nunique()
df_kpi["practice_uuid"].nunique()
df_kpi = df_kpi.merge(df_demo, on="practice_uuid", how="left")

# prep book of business figures
df_con_client_count = df_kpi.copy()
df_con_client_count = df_con_client_count[["consultant", "office_id"]]
df_con_client_count = df_con_client_count.groupby("consultant")["office_id"].nunique()
df_kpi = df_kpi.merge(
    df_con_client_count.rename("con_client_count"), on="consultant", how="left"
)

df_lead_client_count = df_kpi.copy()
df_lead_client_count = df_lead_client_count[["team_lead", "office_id"]]
df_lead_client_count = df_lead_client_count.groupby("team_lead")["office_id"].nunique()
df_kpi = df_kpi.merge(
    df_lead_client_count.rename("lead_client_count"), on="team_lead", how="left"
)

# prep for treatment efficiency kpi
# "\" means code/command/line continues on line below
df_kpi["new_pts_completion_avg_revenue"] = (
    df_kpi["new_completed_amt"] / df_kpi["new_completed_pts"]
)
df_kpi["existing_pts_completion_avg_revenue"] = (
    df_kpi["existing_completed_amt"] / df_kpi["existing_completed_pts"]
)

# prep production and collection tables
df_kpi["yoy_production"] = (
    df_kpi["12_mo_production"] / df_kpi["previous_12_mo_production"]
)
df_kpi["yoy_collection"] = (
    df_kpi["12_mo_collection"] / df_kpi["previous_12_mo_collection"]
)

# calculate kpis, in order from list above
df_kpi["kpi_12_mo_production"] = (
    df_kpi["12_mo_production"] / df_kpi["previous_12_mo_production"] - 1
)

df_kpi["kpi_12_mo_collection"] = (
    df_kpi["12_mo_collection"] / df_kpi["previous_12_mo_collection"] - 1
)

df_kpi["kpi_12_mo_collection_efficiency"] = df_kpi["12_mo_collection"] / (
    df_kpi["12_mo_production"] + df_kpi["12_mo_adjustments"]
)

df_kpi["kpi_30_day_hygiene_production"] = (
    df_kpi["30_day_hygiene_production"] / df_kpi["30_day_production"]
)

df_kpi["kpi_12_mo_hygiene_production"] = (
    df_kpi["12_mo_hygiene_production"] / df_kpi["12_mo_production"]
)

df_kpi["kpi_AR_average_month_production"] = df_kpi["ar_total"] / (
    df_kpi["12_mo_production"] / 12
)

df_kpi["kpi_new_pts_avg_revenue"] = (
    df_kpi["new_scheduled_amt"] / df_kpi["new_scheduled_pts"]
)

df_kpi["kpi_existing_pts_avg_revenue"] = (
    df_kpi["existing_scheduled_amt"] / df_kpi["existing_scheduled_pts"]
)

df_kpi["kpi_new_pts_completion"] = (
    df_kpi["new_pts_completion_avg_revenue"] / df_kpi["kpi_new_pts_avg_revenue"]
)

df_kpi["kpi_existing_pts_completion"] = (
    df_kpi["existing_pts_completion_avg_revenue"]
    / df_kpi["kpi_existing_pts_avg_revenue"]
)

# adjust ch call figures
ch_call_ceiling = ["ch_team_call", "ch_leadership_call"]
for n in ch_call_ceiling:
    df_kpi["color_" + n] = (df_kpi[n].clip(upper=1) * 2) + 1

# calculate kpi colors
df_kpi["client_kpi_average"] = 0
for n in kpi_ranges:
    df_kpi["color_" + n] = np.where(df_kpi[n] < kpi_ranges[n][2], 2, 3)
    df_kpi["color_" + n] = np.where(
        df_kpi[n] < kpi_ranges[n][1], 1, df_kpi["color_" + n]
    )

# calculate ch colors
df_kpi["client_ch_average"] = 0
for n in ch_ranges:
    df_kpi["color_" + n] = np.where(df_kpi[n] >= ch_ranges[n], 3, 1)

# calculate practice health
def practice_health_calc(kpi_weighting=kpi_weighting):
    for n in kpi_weighting:
        df_kpi["client_kpi_average"] = df_kpi["client_kpi_average"] + (
            df_kpi["color_" + n] * kpi_weighting[n]
        )
    df_kpi["client_kpi_average"] = df_kpi["client_kpi_average"].round(1)


practice_health_calc(kpi_weighting)

# calculate customer health
def customer_health_calc(ch_weighting=ch_weighting):
    for n in ch_weighting:
        df_kpi["client_ch_average"] = df_kpi["client_ch_average"] + (
            df_kpi["color_" + n] * ch_weighting[n]
        )
    df_kpi["client_ch_average"] = df_kpi["client_ch_average"].round(1)


customer_health_calc()

df_kpi["client_kpi_average"][:5]
df_kpi["client_ch_average"][:5]

df_kpi.columns
df_kpi.shape

# create client profile
from functools import reduce
import operator


def getFromDict(dataDict, mapList):
    return reduce(operator.getitem, mapList, dataDict)


for n in profile_columns:
    df_kpi["profile_" + n] = "f"
    for i in getFromDict(profile_ranges, [n]):
        df_kpi["profile_" + n] = np.where(
            df_kpi[n] < i, getFromDict(profile_ranges, [n, i]), df_kpi["profile_" + n]
        )
        df_kpi["profile_" + n] = np.where(
            df_kpi[n].isnull(), np.nan, df_kpi["profile_" + n]
        )

df_kpi["client_profile"] = df_kpi["doctor_count"].astype(str) + " ("
for n in profile_columns:
    df_kpi["client_profile"] = (
        df_kpi["client_profile"] + df_kpi["profile_" + n].astype(str) + "/"
    )

df_kpi["client_profile"] = df_kpi["client_profile"] + ")"
df_kpi["client_profile"] = df_kpi["client_profile"].str.replace(".0", "")
df_kpi["client_profile"] = df_kpi["client_profile"].str.replace(r"\/\)", ")")
df_kpi["client_profile"][:10]

# rename to kpi dashboard names
kpi_renames = [
    "color_kpi_12_mo_production",
    "color_kpi_12_mo_collection",
    "color_kpi_12_mo_collection_efficiency",
    "color_kpi_30_day_hygiene_production",
    "color_kpi_12_mo_hygiene_production",
    "color_kpi_AR_average_month_production",
    "color_kpi_new_pts_avg_revenue",
    "color_kpi_existing_pts_avg_revenue",
    "color_kpi_new_pts_completion",
    "color_kpi_existing_pts_completion",
    "client_kpi_average",
]

kpi_display_name = [
    "Production Growth",
    "Collections Growth",
    "Collection Efficiency",
    "30 Day Hygiene",
    "365 Day Hygiene",
    "AR % of Monthly Production",
    "New Patient Revenue",
    "Existing Patient Revenue",
    "New Patient Completion",
    "Existing Patient Completion",
    "Practice Health",
]

format_table_kpi = dict(zip(kpi_renames, kpi_display_name))
df_kpi.rename(columns=format_table_kpi, inplace=True)

ch_renames = [
    "color_ch_team_call",
    "color_ch_leadership_call",
    "color_ch_attended_poe_dates",
    "color_ch_course_attendance",
    "client_ch_average",
]

ch_display_name = [
    "Team Calls",
    "Leadership Calls",
    "POE Attended",
    "SPS Course Attendance",
    "Customer Health",
]

format_table_ch = dict(zip(ch_renames, ch_display_name))
df_kpi.rename(columns=format_table_ch, inplace=True)

df_kpi["refresh_date"] = dt.datetime.today()
df_kpi.columns

demo_columns = [
    "practice_uuid",
    "account_id",
    "office_id",
    "office_name",
    "customer",
    "state",
    "client_enabled",
    "dpms_id",
    "contract_start_date",
    "contract_end_date",
    "is_pending",
    "agreed_to_terms",
    "location_count",
    "team_count",
    "doctor_count",
    "consultant",
    "team_lead",
    "client_profile",
    "profile_team_count",
    "profile_12_mo_production",
    "profile_patient_base",
    "profile_location_count",
]

kpi_buildup_columns = [
    "30_day_production",
    "30_day_hygiene_production",
    "12_mo_production",
    "previous_12_mo_production",
    "12_mo_hygiene_production",
    "12_mo_collection",
    "previous_12_mo_collection",
    "12_mo_adjustments",
    "ar_total",
    "new_planned_opportunity",
    "existing_planned_opportunity",
    "new_scheduled_amt",
    "existing_scheduled_amt",
    "new_completed_amt",
    "existing_completed_amt",
    "new_planned_pts",
    "new_scheduled_pts",
    "new_completed_pts",
    "existing_planned_pts",
    "existing_scheduled_pts",
    "existing_completed_pts",
    "patient_base",
    "new_pts_completion_avg_revenue",
    "existing_pts_completion_avg_revenue",
    "yoy_production",
    "yoy_collection",
]

kpi_columns = [
    "kpi_12_mo_production",
    "kpi_12_mo_collection",
    "kpi_12_mo_collection_efficiency",
    "kpi_30_day_hygiene_production",
    "kpi_12_mo_hygiene_production",
    "kpi_AR_average_month_production",
    "kpi_new_pts_avg_revenue",
    "kpi_existing_pts_avg_revenue",
    "kpi_new_pts_completion",
    "kpi_existing_pts_completion",
    "Production Growth",
    "Collections Growth",
    "Collection Efficiency",
    "30 Day Hygiene",
    "365 Day Hygiene",
    "AR % of Monthly Production",
    "New Patient Revenue",
    "Existing Patient Revenue",
    "New Patient Completion",
    "Existing Patient Completion",
    "Practice Health",
]

ch_columns = [
    "ch_team_call",
    "ch_leadership_call",
    "ch_attended_poe_dates",
    "ch_attended_course_dates",
    "ch_scheduled_course_dates",
    "ch_course_attendance",
    "ch_non_sps_campus_events",
    "Team Calls",
    "Leadership Calls",
    "POE Attended",
    "SPS Course Attendance",
    "Customer Health",
]

keep_columns = demo_columns + kpi_buildup_columns + kpi_columns + ch_columns
df_export = df_kpi[keep_columns].copy()

df_export["Red Clients"] = np.where(
    (df_export["Practice Health"] < 1.7) & (df_export["Customer Health"] < 1.7), 1, 0
)

df_bar_view = df_export.melt(
    id_vars=[
        "team_lead",
        "consultant",
        "practice_uuid",
        "customer",
        "office_name",
        "office_id",
        "state",
        "contract_start_date",
        "contract_end_date",
    ],
    value_vars=[
        "Production Growth",
        "Collections Growth",
        "Collection Efficiency",
        "30 Day Hygiene",
        "365 Day Hygiene",
        "AR % of Monthly Production",
        "New Patient Revenue",
        "Existing Patient Revenue",
        "New Patient Completion",
        "Existing Patient Completion",
        "Practice Health",
        "Team Calls",
        "Leadership Calls",
        "POE Attended",
        "SPS Course Attendance",
        "Customer Health",
        "Red Clients",
    ],
    var_name="Metric",
)

df_bar_view.sort_values(["Metric", "value", "practice_uuid"], inplace=True)
df_bar_view = df_bar_view.reset_index(drop=True)

df_time_data["refresh_date"] = dt.datetime.today()
df_export["refresh_date"] = dt.datetime.today()
df_bar_view["refresh_date"] = dt.datetime.today()

# exports
os.remove(r"A:\data\python_data\sps\reporting_extracts\sps_time.csv")
df_time_data.to_csv(r"A:\data\python_data\sps\reporting_extracts\sps_time.csv")

os.remove(r"A:\data\python_data\sps\reporting_extracts\sps_metrics_base.csv")
df_export.to_csv(r"A:\data\python_data\sps\reporting_extracts\sps_metrics_base.csv")

os.remove(r"A:\data\python_data\sps\reporting_extracts\sps_metrics_visual.csv")
df_bar_view.to_csv(r"A:\data\python_data\sps\reporting_extracts\sps_metrics_visual.csv")

print("Complete")

import sqlalchemy as sa
import psycopg2
import pandas as pd
import numpy as np
import datetime as dt
import os
from keyring import get_password

print("Running...")

# existing sps kpis
df = pd.read_csv(
    r"A:\data\python_data\sps\sps_metrics_new.csv", low_memory=False
)
df.drop(columns=["Unnamed: 0"], inplace=True)
df.columns
df.shape

# campus metrics
df_campus = pd.read_csv(r"A:\data\csv\expCourseAttendees.csv", low_memory=False,)
df_campus.shape
df_campus.columns = df_campus.columns.str.replace(" ", "_").str.lower()
date_cols = ["course_start_date", "campus_sales_order_date"]
for n in date_cols:
    df_campus[n] = pd.to_datetime(df_campus[n])

df_campus["past_days"] = dt.datetime.today() - df_campus["course_start_date"]
df_campus["past_days"] = df_campus["past_days"].dt.round("d")
df_campus["past_days"] = df_campus["past_days"].dt.days.astype("int32")
df_campus.head(30)

# notes, in order:
# sps courses only, remove tax/adjustment rows, non-cancelled, non no-show
df_sps_campus = df_campus[df_campus["campus_event"].str.contains("SPS") == True]
df_sps_campus = df_sps_campus[
    df_sps_campus["attendee_status"].str.contains("Cancelled") == False
]
df_sps_campus = df_sps_campus[df_sps_campus["no-show"].str.contains("No") == True]

df_sps_campus["sps_course"] = df_sps_campus["campus_event"].str[:4]
df_sps_campus.groupby("sps_course")["top_level_parent"].nunique()
df_sps_campus.columns
df_sps_campus.shape

# calcs
half_year_days = 183
year_days = 366
future_year_days = -366

df_sps_campus["ch_attended_poe_dates"] = np.where(
    (
        (df_sps_campus["sps_course"] == "SPS1")
        & (df_sps_campus["course_start_date"] < dt.datetime.today())
    ),
    df_sps_campus["course_start_date"],
    pd.NaT,
)
df_sps_campus["ch_attended_course_dates"] = np.where(
    (
        (df_sps_campus["sps_course"] != "SPS1")
        & (df_sps_campus["course_start_date"] < dt.datetime.today())
        & (df_sps_campus["past_days"] < half_year_days)
    ),
    df_sps_campus["course_start_date"],
    pd.NaT,
)
df_sps_campus["ch_scheduled_course_dates"] = np.where(
    (
        (df_sps_campus["sps_course"] != "SPS1")
        & (df_sps_campus["course_start_date"] > dt.datetime.today())
        & (df_sps_campus["past_days"] > future_year_days)
    ),
    df_sps_campus["course_start_date"],
    pd.NaT,
)

df_ch_campus = (
    df_sps_campus.groupby(["top_level_parent"])
    .agg(
        {
            "ch_attended_poe_dates": "nunique",
            "ch_attended_course_dates": "nunique",
            "ch_scheduled_course_dates": "nunique",
        }
    )
    .reset_index()
)
df_ch_campus.drop_duplicates(inplace=True)

# non-sps events
df_other_campus = df_campus[
    df_campus["campus_event"].str.contains("SPS\d") == False
]  # sps courses only
df_other_campus = df_other_campus[
    df_other_campus["barcode"].str.contains("CE") == False
]  # these are tax/adjustment lines
df_other_campus = df_other_campus[
    df_other_campus["attendee_status"].str.contains("Cancelled") == False
]  # not cancelled
df_other_campus = df_other_campus[
    df_other_campus["no-show"].str.contains("No") == True
]  # not no-show
df_other_campus = df_other_campus[
    df_other_campus["course_start_date"] < dt.datetime.today()
]
df_other_campus = df_other_campus[df_other_campus["past_days"] < year_days]

df_ch_other_campus = (
    df_other_campus.groupby("top_level_parent")
    .agg({"course_start_date": "nunique"})
    .reset_index()
)

df_ch_campus = df_ch_campus.merge(df_ch_other_campus, how="left", on="top_level_parent")
df_ch_campus.rename(
    columns={
        "top_level_parent": "customer",
        "course_start_date": "ch_non_sps_campus_events",
    },
    inplace=True,
)
df_ch_campus.columns

# phone metrics
df_phone = pd.read_csv(r"A:\data\python_data\phone_calls\call_total_sps.csv",)
df_phone.drop(columns=["Unnamed: 0"], inplace=True)
df_phone.columns = df_phone.columns.str.replace(" ", "_").str.lower()
df_phone["contivio_call_type"] = df_phone["contivio_call_type"].str.lower().str.strip()
df_phone["startdate"] = pd.to_datetime(df_phone["startdate"])

from pandas.tseries.offsets import MonthEnd

month_close = dt.datetime.today() + MonthEnd(-1)
three_month_days = 95

df_phone["past_days"] = month_close - df_phone["startdate"]
df_phone["past_days"] = df_phone["past_days"].dt.round("d")
df_phone["past_days"] = df_phone["past_days"].dt.days.astype("float")
df_phone = df_phone[df_phone["past_days"] < three_month_days]
df_phone = df_phone[df_phone["past_days"] > 0]


df_phone["team_call"] = np.where(
    (
        df_phone["contivio_call_type"].str.contains("team")
        & (df_phone["contivio_call_type"].str.contains("cancel") == False)
    ),
    1,
    0,
)

df_phone["leadership_call"] = np.where(
    (
        df_phone["contivio_call_type"].str.contains("leadership")
        & (df_phone["contivio_call_type"].str.contains("cancel") == False)
    ),
    1,
    0,
)

df_phone_sum = (
    df_phone.groupby(["office_id", "customer", "contracts_start", "consultant"])
    .agg({"team_call": "sum", "leadership_call": "sum"})
    .reset_index()
)
df_phone_sum["contracts_start"] = pd.to_datetime(df_phone_sum["contracts_start"])
df_phone_sum["contract_adjusted_days"] = (
    month_close - df_phone_sum["contracts_start"]
) / 30
df_phone_sum["contract_adjusted_days"] = df_phone_sum[
    "contract_adjusted_days"
].dt.round("d")
df_phone_sum["contract_adjusted_days"] = df_phone_sum["contract_adjusted_days"].dt.days
df_phone_sum["contract_adjusted_days"] = np.where(
    df_phone_sum["contract_adjusted_days"] > 3,
    3,
    df_phone_sum["contract_adjusted_days"],
)

df_phone_sum["ch_team_call"] = (
    df_phone_sum["team_call"] / df_phone_sum["contract_adjusted_days"]
)

df_phone_sum["ch_leadership_call"] = (
    df_phone_sum["leadership_call"] / df_phone_sum["contract_adjusted_days"]
)

df_phone_sum = df_phone_sum.merge(df_ch_campus, how="left", on="customer")
df_phone_sum["ch_attended_poe_dates"].isna().sum()
df_phone_sum["customer"].nunique()

df_phone_sum["ch_course_attendance"] = np.where(
    (df_phone_sum["ch_attended_course_dates"] > 0)
    | (df_phone_sum["ch_scheduled_course_dates"] > 0),
    1,
    0,
)

df_ch = df_phone_sum[
    [
        "office_id",
        "ch_team_call",
        "ch_leadership_call",
        "ch_attended_poe_dates",
        "ch_attended_course_dates",
        "ch_scheduled_course_dates",
        "ch_course_attendance",
        "ch_non_sps_campus_events",
    ]
]

df = df.merge(df_ch, how="left", on="office_id")

os.remove(r"A:\data\python_data\sps\sps_metrics_with_ch.csv")
df.to_csv(r"A:\data\python_data\sps\sps_metrics_with_ch.csv")

print("Complete")

"""
aliases
Percent of Team Calls
Percent of Leadership Calls
Practice of Excellence Attended
SPS Courses Attended Last 365 Days
Other SPEAR Courses Attended Last 365 Days
SPS Courses Scheduled Next 365 Days
"""
